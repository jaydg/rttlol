import std.conv;
import std.json;
import std.math;

public struct Position
{
    struct Direction {
        public int dx;
        public int dy;

        public this(int dx, int dy) {
            this.dx = dx;
            this.dy = dy;
        }
    }

    private static enum _directions = [
        "n":  Direction(0, -1),
        "ne": Direction(1, -1),
        "e":  Direction(1, 0),
        "se": Direction(1, 1),
        "s":  Direction(0, 1),
        "sw": Direction(-1, 1),
        "w":  Direction(-1, 0),
        "nw": Direction(-1, -1)
    ];

    private int _x = int.max;
    private int _y = int.max;

    public @property {
        int x() {
            return _x;
        }
        int x(int newX) {
            return _x = newX;
        }
        int y() {
            return _y;
        }
        int y(int newY) {
            return _y = newY;
        }

        static string[] directions() {
            return _directions.keys;
        }
    }

    public this(int x, int y) {
        _x = x;
        _y = y;
    }

    public this(Position pos) {
        _x = pos.x;
        _y = pos.y;
    }

    public this(JSONValue json) {
        _x = cast(int)json["x"].integer;
        _y = cast(int)json["y"].integer;
    }

    public bool invalid() {
        return _x == int.max && _y == int.max;
    }

    public string toString() {
        return "(" ~ to!string(x) ~ "," ~ to!string(y) ~ ")";
    }

    JSONValue toJSON() {
        JSONValue[string] json;
        json["x"] = _x;
        json["y"] = _y;

        return JSONValue(json);
    }

    public bool adjacent(Position pos)
    in {
        assert(!pos.invalid);
    } body {
        return distance(pos) == 1;
    }

    public long distance(Position pos)
    in {
        assert(!pos.invalid);
    } body {
        int dx = _x - pos.x;
        int dy = _y - pos.x;
        int dist = dx * dx + dy * dy;

        return cast(long) round(sqrt(cast(float) dist));
    }

    public Position move(string dir)
    in {
        assert(dir !is null);
        assert(dir in _directions);
    } body {
        _x += _directions[dir].dx;
        _y += _directions[dir].dy;

        return this;
    }
}
