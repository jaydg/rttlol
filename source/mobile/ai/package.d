module mobile.ai;

import std.array : split;
import std.json;

import mobile.monster;
import mobile.ai.basic;

public class AI
{
    abstract JSONValue toJSON() {
        JSONValue[string] json;
        json["kind"] = this.classinfo.name.split('.')[$-1];

        return JSONValue(json);
    }

    static AI fromJSON(JSONValue json) {
        return AI.create(json["kind"].str, json);
    }

    static public AI create(string kind, JSONValue json=null) {
        switch (kind) {
            case "BasicAI":
                return new BasicAI(json);
            default:
                throw new Exception("Unknown AI '" ~ kind ~ "'");
        }
    }

    abstract string getDirection();
}
