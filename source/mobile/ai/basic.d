﻿module mobile.ai.basic;

import std.json;
import std.random;

import mobile.ai;
import position;

class BasicAI : AI {
    this(JSONValue json=null) {
    }

    override JSONValue toJSON() {
        return super.toJSON();
    }

    override string getDirection()
    {
        return Position.directions[uniform(0, Position.directions.length)];
    }
}