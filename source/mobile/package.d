module mobile;

import std.experimental.logger: logf, LogLevel;
import std.json;
import std.math;
import std.typecons;

import drawable;
import game;
import map;
import position;

public abstract class Mobile : Drawable {
    private static ulong maxid;
    private ulong _id;
    private Map _map = null;
    private Position _pos;
    private bool[][] visibility = null;
    private size_t _speed;
    private Game.runfun _pending_action;

    JSONValue toJSON() {
        JSONValue[string] json;
        json["map"] = _map.name;
        json["position"] = _pos.toJSON;
        json["speed"] = _speed;
        json["id"] = _id;

        return JSONValue(json);
    }

    protected this() {
        this._id = maxid++;
    }

    protected this(JSONValue json) {
        speed = cast(size_t)json["speed"].integer;
        _id = json["id"].integer;

        if (_id >= maxid) {
            maxid = _id + 1;
        }
    }

    public Map setMap(Map map) {
        Position dest;
        if (map !is null)
            dest = map.findExit(_map.name);

        // findExit can return null when the exit cannot be found
        if (dest.invalid)
            dest = map.findSpace();

        return setMap(map, dest);
    }

    public Map setMap(Map map, Position pos) {
        if (_map !is null && !_pos.invalid) {
            // remove the mobile from its previous tile on the previous map
            _map.at(_pos).mobile = null;
        }

        _map = map;
        _pos = pos;

        if (map !is null && !pos.invalid) {
            // place the mobile on the given map at the given position
            map.at(pos).mobile = this;
        }

        return _map;
    }

    @property {
        public Map map() {
            return _map;
        }

        public Position pos() {
            return this._pos;
        }

        public Position pos(Position pos)
        in {
            assert(_map.at(pos).mobile is null);
            assert(!pos.invalid);
        } body {
            // Remove the mobile from the current tile
            _map.at(_pos).mobile = null;
            _pos = pos;
            // Put it onto the new tile
            _map.at(_pos).mobile = this;
            return _pos;
        }

        protected size_t speed(size_t s) {
            return _speed = s;
        }

        public size_t speed() {
            return _speed;
        }

        public bool canMove() {
            return _pending_action == null;
        }

        public ulong id() {
            return this._id;
        }

        abstract public string name();
        abstract public string description();
    }

    package Tuple!(Position, size_t) prepareMove(string dir) {
        Position pos = this._pos;
        pos.move(dir);

        if (_map.isValid(pos) && _map.at(pos).isPassable) {
            size_t movement_cost = this.map.at(this.pos).movementCost;

            // map scale
            movement_cost *= this.map.scale;

            // mobile speed
            movement_cost *= 100;
            movement_cost /= this.speed;

            return tuple(pos, movement_cost);
        } else {
            return tuple(this._pos, cast(size_t)0);
        }
    }

    public void enqueueAction(ulong duration, Game.runfun action) in {
        assert(_pending_action == null);
    } body {
        logf(LogLevel.trace, "Movement queued for %d at tick %d, duration %d",
            this.id, Game.get.tick, duration);

        _pending_action = action;
        Game.get.enqueue(duration, {
                action();
                _pending_action = null;
            });
    }

    // FOV stuff
    // adapted from http://www.roguebasin.com/index.php?title=C%2B%2B_shadowcasting_implementation
    private void castLight(uint x, uint y, uint radius, uint row,
        double startSlope, double endSlope, uint xx, uint xy,
        uint yx, uint yy) {
        if (startSlope < endSlope) {
            return;
        }

        double nextStartSlope = startSlope;

        foreach (immutable i; row .. radius + 1) {
            bool blocked;
            for (int dx = -i, dy = -i; dx <= 0; dx++) {
                const double lSlope = (dx - 0.5) / (dy + 0.5);
                const double rSlope = (dx + 0.5) / (dy - 0.5);
                if (startSlope < rSlope) {
                    continue;
                } else if (endSlope > lSlope) {
                    break;
                }

                const int sax = dx * xx + dy * xy;
                const int say = dx * yx + dy * yy;
                if ((sax < 0 && abs(sax) > x) ||
                    (say < 0 && abs(say) > y)) {
                    continue;
                }
                uint ax = x + sax;
                uint ay = y + say;
                if (ax >= _map.size_x || ay >= _map.size_y) {
                    continue;
                }

                if ((dx * dx + dy * dy) < radius * radius) {
                    visibility[ay][ax] =  true;
                }

                if (blocked) {
                    if (! _map.at(ay, ax).isTransparent) {
                        nextStartSlope = rSlope;
                        continue;
                    } else {
                        blocked = false;
                        startSlope = nextStartSlope;
                    }
                } else if (! _map.at(ay, ax).isTransparent) {
                    blocked = true;
                    nextStartSlope = rSlope;
                    castLight(x, y, radius, i + 1, startSlope,
                        lSlope, xx, xy, yx, yy);
                }
            }
            if (blocked) {
                break;
            }
        }
    }

    public void calculateFov(uint radius) {
        const int[][] multipliers = [
            [1, 0, 0, -1, -1, 0, 0, 1],
            [0, 1, -1, 0, 0, -1, 1, 0],
            [0, 1, 1, 0, 0, -1, -1, 0],
            [1, 0, 0, 1, -1, 0, 0, -1]
        ];

        if ((visibility == null)
            || visibility.length != map.size_y
            || visibility[0].length != map.size_x) {
            // Create a new visibility map for the current map
            visibility = new bool[][](map.size_y, map.size_x);
        } else {
            // reset the existing visibility map
            foreach (immutable y; 0 .. visibility.length) {
                foreach (immutable x; 0 .. visibility[0].length) {
                    visibility[y][x] = false;
                }
            }
        }

        // the mobile's position is visible, too
        if (radius > 0) {
            visibility[pos.y][pos.x] = true;
        }

        foreach (immutable i; 0 .. 8) {
            castLight(pos.x, pos.y, radius, 1, 1.0, 0.0,
                multipliers[0][i], multipliers[1][i],
                multipliers[2][i], multipliers[3][i]);
        }
    }

    public bool canSee(int y, int x)
    in {
        assert(visibility != null);
        assert(visibility.length >= y);
        assert(visibility[0].length >= x);
    } body {
        return visibility[y][x];
    }

    public bool canSee(Position pos)
    in {
        assert(!pos.invalid);
    } body {
        return canSee(pos.y, pos.x);
    }
}
