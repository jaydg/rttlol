module mobile.monster;

import std.experimental.logger: log, logf, LogLevel;
import std.format;
import std.json;
import std.typecons;
import toml;

import game;
import map;
import mobile;
import mobile.ai;
import position;

class Monster : Mobile {
    private struct MonsterData {
        char glyph;
        string color;
        string name;
        string description;
        string ai;
        size_t speed;
    }

    private static MonsterData[string] data;

    private static loadData() {
        // Load the monster data
        log(LogLevel.info, "Loading monster data.");
        import std.file : read;

        try {
            auto monsterconfig = parseTOML(cast(string)read("data/monsters.conf"));

            // Find all tile type definitions
            foreach (mtd; monsterconfig.keys) {
                log(LogLevel.info, "Found monster type data: ", mtd);
                MonsterData md = MonsterData();
                md.glyph       = monsterconfig[mtd]["glyph"].str[0];
                log(LogLevel.info, "\tglyph:       ", md.glyph);
                md.color       = monsterconfig[mtd]["color"].str;
                log(LogLevel.info, "\tcolor:       ", md.color);
                md.name        = monsterconfig[mtd]["name"].str;
                log(LogLevel.info, "\tname:        ", md.name);
                md.description = monsterconfig[mtd]["description"].str;
                log(LogLevel.info, "\tdescription: ", md.description);
                md.ai          = "ai" in monsterconfig[mtd] ? monsterconfig[mtd]["ai"].str : "BasicAI";
                log(LogLevel.info, "\tai:          ", md.ai);
                md.speed       = cast(size_t)monsterconfig[mtd]["speed"].integer;
                log(LogLevel.info, "\tspeed:       ", md.speed);

                data[mtd] = md;
            }
        } catch (Exception e) {
            log(LogLevel.critical, "Error: ", e.msg);
        }

        // Optimize the array before usage
        data.rehash;
    }

    private string _kind;
    private AI _ai;

    public this(string kind, Map map, Position pos) {
        super();

        if (!data.length) {
            this.loadData();
        }

        if (! (kind in data)) {
            throw new Exception(format("The monster '%s' is not known!", kind));
        }

        this._kind = kind;
        this._ai = AI.create(data[kind].ai);
        this.setMap(map, pos);
    }

    public this(JSONValue json, Map map) {
        super(json);

        if (!data.length) {
            this.loadData();
        }

        this._kind = json["kind"].str;
        this._ai = AI.fromJSON(json["ai"]);
        setMap(map, Position(json["position"]));
    }

    public override JSONValue toJSON() {
        JSONValue json = super.toJSON();
        json["kind"] = JSONValue(_kind);
        json["ai"] = _ai.toJSON();

        return JSONValue(json);
    }

    @property {
        public string kind() {
            return _kind;
        }

        public override string name() {
            return data[_kind].name;
        }

        public override string description() {
            return data[_kind].description;
        }

        public override size_t speed() {
            return data[_kind].speed;
        }

        // from Drawable interface
        public char glyph() {
            return data[_kind].glyph;
        }

        public string color() {
            return data[_kind].color;
        }
    } // @property

    public override Map setMap(Map map, Position pos) {
        if (this.map) {
            this.map.removeMonster(this);
        }
        map.addMonster(this);

        return super.setMap(map, pos);
    }

    public bool move() {
        logf(LogLevel.trace, "Scheduling monster %d move at tick %d",
            this.id, Game.get.tick);

        string dir = _ai.getDirection();
        // new position and time required to reach it
        Tuple!(Position, size_t) result = prepareMove(dir);

        if (result[0] != this.pos && result[1] != 0) {
            enqueueAction(result[1], {
                    logf(LogLevel.trace, "Moving monster %d at tick %d",
                        this.id, Game.get.tick);

                    if (!map.at(result[0]).isPassable) {
                        // destination is no longer passable
                        logf(LogLevel.trace, "%d cannot move", this.id);
                    } else {
                        logf(LogLevel.trace, "%d moves from %s to %s", this.id, this.pos, result[0]);
                        this.pos = result[0];
                    }
                });
            return true;
        } else {
            // rest a turn
            enqueueAction(data[_kind].speed, {});
            logf(LogLevel.trace, "Nothing queued for %d", this.id);
            return false;
        }
    }
}
