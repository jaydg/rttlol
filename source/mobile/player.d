module mobile.player;

import std.experimental.logger : logf, LogLevel;
import std.json;
import std.typecons;

import game;
import map;
import mobile;
import position;

public class Player : Mobile {
    char[][][string] memory;
    private const int baseVisibleRadius = 80;

    override JSONValue toJSON() {
        JSONValue json = super.toJSON();
        json["memory"] = JSONValue(memory);

        return JSONValue(json);
    }

    public this() {
        super();

        this.speed = 100;
    }

    public this(JSONValue json, Map[string] maps) {
        super(json);
        setMap(maps[json["map"].str], Position(json["position"]));

        foreach (name, mem; json["memory"].object) {
            const size_t size_y = mem.array.length;
            const size_t size_x = mem.array[0].str.length;
            memory[name] = new char[][](size_y, size_x);

            int y;
            foreach (row; mem.array) {
                int x;
                foreach(char cell; row.str) {
                    memory[name][y][x++] = cell;
                }
                y++;
            }
        }
    }

    public override string name() {
        return "You";
    }

    public override string description() {
        return "yes, you";
    }

    public override Map setMap(Map map) {
        Game.get.history.write!"You enter %s."(map.description);
        return super.setMap(map);
    }

    public override Map setMap(Map map, Position pos) {
        Map m = super.setMap(map, pos);

        // create memory for this map
        if (memory.get(m.name, null) is null) {
            memory[m.name] = new char[][](m.size_y, m.size_x);
            foreach (immutable y; 0 .. map.size_y) {
                foreach (immutable x; 0 .. map.size_x) {
                    memory[m.name][y][x] = ' ';
                }
            }
        }

        return m;
    }

    public bool move(string dir) {
        logf(LogLevel.trace, "Scheduling player move at tick %d",
            Game.get.tick);

        // new position and time required to reach it
        Tuple!(Position, size_t) result = prepareMove(dir);
        logf(LogLevel.trace, "prepareMove returned new position %s, movement cost: %d",
            result[0], result[1]);

        if (result[0] != this.pos && result[1] != 0) {
            enqueueAction(result[1], {
                    logf(LogLevel.trace, "moving player at tick %d to %s", Game.get.tick, result[0]);
                    if (map.at(result[0]).mobile) {
                        // TODO: attack mob
                        logf(LogLevel.trace, "Aborting player move at %d, pos taken", Game.get.tick);
                    } else {
                        this.pos = result[0];
                    }
                });
            return true;
        } else {
            return false;
        }
    }

    public void calculateFov() {
        const uint radius = (this.baseVisibleRadius * this.map.illumination) / 100;
        super.calculateFov(radius);

        // update the player's memory of the map
        foreach (immutable y; 0 .. map.size_y) {
            foreach (immutable x; 0 .. map.size_x) {
                if (y == pos.y && x == pos.x)
                    continue;

                if (canSee(y, x))
                    memory[map.name][y][x] = map.at(y, x).glyph;
            }
        }
    }

    // From interface Drawable:
    public char glyph() {
        return '@';
    }

    public string color() {
        return "dfdf5f";
    }
}
