module util.dice;

import std.conv;
import std.random;
import std.regex;

class Dice {
    static auto re = ctRegex!(r"^([0-9]*)d([0-9]+)([\+-][0-9]+)?$");

    int roll(string expression) {
        auto c = matchFirst(expression, re);

        int count = c[1] != "" ? to!int(c[1]) : 1;
        int dice = to!int(c[2]);
        int add = c[3] != "" ? to!int(c[3]) : 0;

        int res = add;
        foreach(immutable i; 0 .. count)
            res += uniform!"[]"(1, dice);

        return res;
    }
}
