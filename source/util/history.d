﻿module util.history;

import std.container.array;
import std.experimental.logger: logf, LogLevel;
import std.format : format;
import std.json;

import game;

public class HistoryEntry {
    ulong tick;
    string time;
    string message;

    @disable this();

    package this(ulong tick, string time, string message) {
        this.tick = tick;
        this.time = time;
        this.message = message;
    }

    package this(JSONValue json) {
        this.tick = json["tick"].integer;
        this.time = json["time"].str;
        this.message = json["message"].str;
    }

    package JSONValue toJSON() {
        JSONValue json;

        json["tick"] = tick;
        json["time"] = time;
        json["message"] = message;

        return json;
    }
}

class History
{
    private Array!HistoryEntry entries;
    public static const maxEntries = 100;

    public this() {
        entries = Array!HistoryEntry();
        entries.reserve(maxEntries);
    }

    public this(JSONValue json) {
        this();
        foreach (jentry; json.array) {
            HistoryEntry entry = new HistoryEntry(jentry);
            entries.insertBack(entry);
        }
    }

    JSONValue toJSON() {
        JSONValue[] json;

        foreach(entry; entries) {
            json ~= entry.toJSON();
        }

        return JSONValue(json);
    }

    public void write(string fmt, Args...)(Args args) {
        Game game = Game.get();
        ulong tick = game is null ? 0 : game.tick;
        string message = format(fmt, args);

        if (latest !is null && latest.tick == tick) {
            latest.message ~= " " ~ message;
        } else {
            if (entries.length == maxEntries) {
                entries.removeBack();
            }

            HistoryEntry entry = new HistoryEntry(tick,
                game is null ? "00:00:00" : game.clock.toString,
                message);
            entries.insertBefore(entries[0..$], entry);
        }

        logf(LogLevel.trace, "History entry: '%s', tick: %d", message, tick);
    }

    public HistoryEntry latest() {
        if (entries.length) {
            return entries.front;
        } else {
            return null;
        }
    }

    public int opApply(scope int delegate(ref HistoryEntry) dg) {
        int result;

        foreach (ref value; this.entries)
        {
            result = dg(value);
            if (result)
                break;
        }

        return result;
    }
}

