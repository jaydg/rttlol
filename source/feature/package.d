module feature;

import std.conv;
import std.json;

import drawable;
import feature.door;
import feature.map_changer;
import mobile;

class Feature {
    protected char _glyph;
    protected string _color;
    protected string _description;

    @property {
        // From interface Drawable:
        public char glyph() {
            return _glyph;
        }

        public string color() {
            return _color;
        }

        public string description() {
            return _description;
        }

        // assume that map features can be passed by default
        public bool passable() {
            return true;
        }

        // assume that map features are transparent by default
        public bool transparent() {
            return true;
        }
    }

    protected this(string[string] params)
    {
        foreach (string param; ["color", "description", "glyph"]) {
            if (! (param in params))
                throw new Exception("Missing parameter '" ~ param ~ "'");
        }

        // assign the common attributes here
        _color = params["color"];
        _description = params["description"];
        _glyph = params["glyph"][0];
    }

    static public Feature create(string kind, string[string] params) {
        Feature feat;

        switch(kind) {
            case "door":
                feat = new Door(params);
                break;
            case "map_changer":
                feat = new MapChanger(params);
                break;
            default:
                throw new Exception("Unknown map feature '" ~ kind ~ "'");
        }

        return feat;
    }

    abstract public void activate(Mobile mob);

    abstract JSONValue toJSON() {
        JSONValue[string] json;
        json["glyph"] = JSONValue(to!string(_glyph));
        json["color"] = JSONValue(_color);
        json["description"] = JSONValue(_description);
        return JSONValue(json);
    }

    static Feature fromJSON(JSONValue json) {
        string[string] params;

        foreach (key, value; json.object) {
            if (key == "kind")
                continue;
            else
                params[key] = value.str;
        }

        return Feature.create(json["kind"].str, params);
    }

}
