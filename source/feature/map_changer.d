module feature.map_changer;

import std.experimental.logger: log, logf, LogLevel;
import std.json;

import feature;
import game;
import mobile;
import map;

class MapChanger : Feature {
    private string _dest;

    package this(string[string] params) {
        super(params);

        foreach (string param; ["destination"]) {
            if (! (param in params))
                throw new Exception("Missing parameter '" ~ param ~ "'");
        }

        _dest = params["destination"];
    }

    @property {
        public string destination() {
            return _dest;
        }
    }

    override JSONValue toJSON() {
        JSONValue json = super.toJSON;
        json["kind"] = JSONValue("map_changer");
        json["destination"] = JSONValue(_dest);

        return JSONValue(json);
    }

    public override void activate(Mobile mob) {
        Game g = Game.get();

        if (mob == g.player) {
            g.save(true);
        }

        logf(LogLevel.trace, "Mobile #%d switching map from '%s' to '%s'.",
            mob.id, mob.map.name, this._dest);
        mob.setMap(g.map(this._dest));
    }
}
