module feature.door;

import std.json;

import feature;
import mobile;

class Door: Feature
{
    private State _state;

    enum State {
        closed,
        open
    }

    package this(string[string] params) {
        super(params);

        if ("state" in params) {
            _state = (params["state"] == "open") ? State.open : State.closed;
        } else {
            _state = State.closed;
        }
    }

    @property {
        public override char glyph() {
            return (_state == State.open) ? '/' : _glyph;
        }

        public State state() {
            return _state;
        }

        public override bool passable() {
            return (_state == State.open) ? true : false;
        }

        public override bool transparent() {
            return (_state == State.open) ? true : false;
        }
    }

    override JSONValue toJSON() {
        JSONValue json = super.toJSON;
        json["kind"] = JSONValue("door");
        json["state"] = (_state == State.open) ? "open" : "closed";

        return JSONValue(json);
    }

    public override void activate(Mobile mob) {
        // FIXME: need to check for mobile on tile
        if (_state == State.closed) {
            _state = State.open;
        } else {
            _state = State.closed;
        }
    }
}
