// This is the Larn maze generator.
// It needs odd width && height to use the entire map.

module map.larn;

import std.random;
import std.conv;
import map;

class LarnMap : Map {
    private void eat(int x, int y) {
        int attempt = 2;
        int dir = uniform(0, 4);

        while (attempt > 0) {
            switch (dir) {
                // west
                case 0:
                    if ((x > 2)
                            && (this.at(y, x - 1).kind == "wall")
                            && (this.at(y, x - 2).kind == "wall")) {
                        this.at(y, x - 1).kind = "floor";
                        this.at(y, x - 2).kind = "floor";
                        eat(x - 2, y);
                    }
                break;

                // east
                case 1:
                    if ((x < (this.size_x - 3))
                            && (this.at(y, x + 1).kind == "wall")
                            && (this.at(y, x + 2).kind == "wall")) {
                        this.at(y, x + 1).kind = "floor";
                        this.at(y, x + 2).kind = "floor";
                        eat(x + 2, y);
                    }
                break;

                // south
                case 2:
                    if ((y > 2)
                            && (this.at(y - 1, x).kind == "wall")
                            && (this.at(y - 2, x).kind == "wall")) {
                        this.at(y - 1, x).kind = "floor";
                        this.at(y - 2, x).kind = "floor";
                        eat(x, y - 2);
                    }
                break;

                // north
                case 3:
                    if ((y < (this.size_y - 3))
                            && (this.at(y + 1, x).kind == "wall")
                            && (this.at(y + 2, x).kind == "wall")) {
                        this.at(y + 1, x).kind = "floor";
                        this.at(y + 2, x).kind = "floor";
                        eat(x, y + 2);
                    }
                break;

                default:
                    assert(0);
            } // switch

            if (++dir > 3) {
                dir = 0;
                attempt--;
            }
        } // while
    }

    package this(string[string] params)
    {
        foreach (string param; ["size_y", "size_x"]) {
            if (! (param in params))
                throw new Exception("Missing parameter '%s'", param);
        }

        super(to!int(params["size_x"]), to!int(params["size_y"]), "wall");

        eat(1, 1);

        // add some free spaces
        int lower = (this.size_x + this.size_y) / 21;
        int upper = (this.size_x + this.size_y) / 11;
        int nrooms = uniform(lower, upper);

        foreach (immutable room; 0 .. nrooms) {
            int cy = uniform(4, this.size_y - 4);
            int cx = uniform(6, this.size_x - 15);

            foreach (immutable y; cy - uniform(1, 3) ..  cy + uniform(1, 3)) {
                foreach (immutable x; cx - uniform(1, 5) .. cx + uniform(1, 14)) {
                     this.at(y, x).kind = "floor";
                }
            }
        }
    }
}
