// Perlin noise terrain generator
// source adapted from https://github.com/nsf/pnoise/blob/master/test.d
module map.terrain;

import std.random;
import std.conv;
import std.math;

import map;

class TerrainMap : Map {
    private struct Noise2DContext {
        Vec2[256] rgradients;
        uint[256] permutations;

    private:

        struct Vec2 {
            float x, y;
        }

        Vec2 get_gradient(immutable int x, immutable int y) pure nothrow
        {
            immutable idx = permutations[x & 255] + permutations[y & 255];
            return rgradients[idx & 255];
        }

        Vec2[8] get_gradients(immutable float x, immutable float y) nothrow
        {
            float x0f = floor(x);
            float y0f = floor(y);
            int x0 = cast(int)x;
            int y0 = cast(int)y;
            int x1 = x0 + 1;
            int y1 = y0 + 1;

            return cast(Vec2[8]) [get_gradient(x0, y0),
            get_gradient(x1, y0),
            get_gradient(x0, y1),
            get_gradient(x1, y1),
            Vec2(x0f + 0.0f, y0f + 0.0f),
            Vec2(x0f + 1.0f, y0f + 0.0f),
            Vec2(x0f + 0.0f, y0f + 1.0f),
            Vec2(x0f + 1.0f, y0f + 1.0f)];
        }

        static float lerp(immutable float a, immutable float b, immutable float v) pure nothrow
        {
            return a * (1 - v) + b * v;
        }

        static float smooth(immutable float v) pure nothrow
        {
            return v * v * (3 - 2 * v);
        }

        static Vec2 random_gradient()
        {
            immutable v = uniform(0.0f, cast(float)PI * 2.0f);
            return Vec2(cos(v), sin(v));
        }

        static float gradient(immutable Vec2 orig, immutable Vec2 grad, immutable Vec2 p) pure nothrow
        {
            immutable sp = Vec2(p.x - orig.x, p.y - orig.y);
            return grad.x * sp.x + grad.y * sp.y;
        }

    public:
        static Noise2DContext opCall()
        {
            Noise2DContext ret;
            foreach (ref elem; ret.rgradients)
                elem = random_gradient();

            foreach (i; 0 .. ret.permutations.length) {
                uint j = uniform(0, cast(uint)i+1);
                ret.permutations[i] = ret.permutations[j];
                ret.permutations[j] = cast(uint)i;
            }

            return ret;
        }

        float get(immutable float x, immutable float y) nothrow
        {
            immutable p = Vec2(x, y);

            immutable vecs = get_gradients(x, y);
            immutable v0 = gradient(vecs[4], vecs[0], p);
            immutable v1 = gradient(vecs[5], vecs[1], p);
            immutable v2 = gradient(vecs[6], vecs[2], p);
            immutable v3 = gradient(vecs[7], vecs[3], p);

            immutable fx = smooth(x - vecs[4].x);
            immutable vx0 = lerp(v0, v1, fx);
            immutable vx1 = lerp(v2, v3, fx);
            immutable fy = smooth(y - vecs[4].y);
            return lerp(vx0, vx1, fy);
        }
    }

    private string elevationToTerrain(float elevation)
    {
        switch(cast(int)(elevation * 100)) {
                case 0:  .. case 30: return "water";
                case 31:             return "shallow_water";
                case 32: .. case 33: return "sand";
                case 34: .. case 36: return "dirt";
                case 37: .. case 50: return "grassland";
                case 51: .. case 60: return "forest";
                case 61: .. case 70: return "hill";
                default:             return "mountain";
        }
    }

    package this(string[string] params)
    {
        foreach (string param; ["size_y", "size_x"]) {
            if (! (param in params))
                throw new Exception("Missing parameter '%s'", param);
        }

        super(to!int(params["size_x"]), to!int(params["size_y"]));

        auto n2d = Noise2DContext();
        auto pixels = new float[this.size_y * this.size_x];

        foreach (immutable y; 0 .. this.size_y) {
            foreach (immutable x; 0 .. this.size_x) {
                immutable v = n2d.get(x * 0.1f, y * 0.1f) * 0.5f + 0.5f;
                pixels[y * this.size_x + x] = v;
            }
        }

        foreach (immutable y; 0 .. this.size_y) {
            float fy = cast(float)(y - (this.size_y / 2)) / (this.size_y / 2);
            foreach (immutable x; 0 .. this.size_x) {
                float val = pixels[y * this.size_x + x];
                float fx = cast(float)(x - (this.size_x / 2)) / (this.size_x / 2);
                pixels[y * this.size_x + x] = val * (2 * cos(fx) - 1) * (2 * cos(fy) - 1) + 0.2;
            }
        }

        foreach (immutable y; 0 .. this.size_y) {
            foreach (immutable x; 0 .. this.size_x) {
                this.at(y, x).kind = elevationToTerrain(pixels[y * this.size_x + x]);
            }
        }
    }
}
