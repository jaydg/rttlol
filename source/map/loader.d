module map.loader;

import std.algorithm;
import std.conv;
import std.experimental.logger: log, logf, LogLevel;
import std.string;
import toml;

import feature;
import map;
import map.delve;
import map.larn;
import map.terrain;
import map.spawn;
import position;

class MapLoader {
    private string name;
    private string filename;
    private TOMLDocument config;

    package this(string _name)
    {
        import std.file : read;

        name = _name;
        filename = "data/maps/" ~ name ~ ".conf";

        config = parseTOML(cast(string)read(filename));

        // ensure the map section is present
        if (! ("map" in config))
            throw new Exception("Missing 'map' section in " ~ filename);

        logf(LogLevel.info, "Loaded map file %s", filename);
    }

    package Map createMap() {
        Map map;

        auto mapconfig = config["map"];
        // Either the map config defines a generator or a layout
        if ("generator" in mapconfig)
            map = generateMap(mapconfig);
        else if ("layout" in mapconfig)
            map = parseMap(mapconfig);
        else
            throw new Exception("Either 'generator' or 'layout' must be specified in "
                ~ filename);

        // optional map scale
        if ("scale" in mapconfig)
            map.scale = cast(int)mapconfig["scale"].integer;
        else
            map.scale = 1;

        // light and darkness
        if ("illumination" in mapconfig)
            map._illumination = cast(uint)mapconfig["illumination"].integer;

        // optional starting position
        if ("start_x" in mapconfig && "start_y" in mapconfig)
            map._starting_position = Position(
                to!int(mapconfig["start_x"].integer),
                to!int(mapconfig["start_y"].integer));

        map.name = name;
        if ("description" in mapconfig)
            map.description = mapconfig["description"].str;

        if ("monsters" in mapconfig) {
            map.spawn = new Spawn(mapconfig["monsters"]);
            map.spawn.spawn(map);
        }

        return map;
    }

    private Map generateMap(TOMLValue mapconfig) {
        if (! ("config" in mapconfig)) {
            throw new Exception(filename ~ "lacks the [map.config] section!"
                ~ filename);
        }

        string[string] params = getParams(mapconfig["config"]);

        Map map;
        Position exit_pos;
        do {
            switch(mapconfig["generator"].str) {
                case "delve":
                    map = new DelveMap(params);
                    break;
                case "larn":
                    map = new LarnMap(params);
                    break;
                case "terrain":
                    map = new TerrainMap(params);
                    break;
                default:
                    throw new Exception("Unknown map generator '"
                        ~ mapconfig["generator"].str ~ "'");
            }

            // add features
            if ("features" in mapconfig) {
                foreach (feature; mapconfig["features"].array) {
                    logf(LogLevel.info, "Adding feature '%s'", feature.str);

                    Position new_exit_pos = addFeature(map, feature.str, config["feature"][feature.str]);
                    if (!new_exit_pos.invalid) {
                        exit_pos = new_exit_pos;
                    }
                }
            }
        } while (! map.validate(exit_pos));

        return map;
    }

    private Map parseMap(TOMLValue mapconfig) {
        // build glyph to tile kind map
        if ("tiles" !in mapconfig) {
            throw new Exception(filename ~ ": missing section [map.tiles].");
        }

        string[char] tilemap;
        foreach (kind, glyph; mapconfig["tiles"]) {
            tilemap[glyph.str[0]] = kind;
        }

        // build glyph to feature map
        string[char] features;
        if ("features" in mapconfig) {
            foreach (kind, glyph; mapconfig["features"]) {
                features[glyph.str[0]] = kind;
                if (kind !in config["feature"]) {
                    throw new Exception(filename ~ ": missing section [feature."
                        ~ kind ~ "].");
                }
                if ("tile_kind" !in config["feature"][kind]) {
                    throw new Exception(filename ~ ": missing 'tile_kind' attribute"
                        ~ " in section [feature." ~ kind ~ "].");
                }
            }
        }

        // parse map
        if ("layout" !in mapconfig) {
            throw new Exception(filename ~ ": missing 'layout' attribute"
                ~ " in section [map].");
        }

        string[] lines = strip(mapconfig["layout"].str).splitLines();
        Map map = new Map(cast(int)lines[0].length, cast(int)lines.length);

        int y = 0;
        foreach(string line; lines) {
            if (map.size_x != line.length)
                throw new Exception(filename ~ ": [map.layout] line length"
                    ~ " mismatch line " ~ to!string(y));

            int x = 0;
            foreach(char cell; line) {
                Position pos = Position(x, y);

                if (cell in features) {
                    // set the tile kind below the feature
                    map.at(pos).kind = config["feature"][features[cell]]["tile_kind"].str;

                    // add the feature
                    addFeature(map, features[cell], config["feature"][features[cell]], pos);
                }
                else if (cell in tilemap) {
                    map.at(pos).kind = tilemap[cell];
                }
                else {
                    throw new Exception(filename ~ ": [map.layout] got undefined"
                        ~ " glyph '" ~ cell ~ "', line " ~ to!string(y)
                        ~ " column " ~ to!string(x));
                }
                x++;
            }
            y++;
        }

        return map;
    }

    private Position addFeature(Map map, string name, TOMLValue featureconf, Position pos=Position()) {
        string[string] params = getParams(featureconf, ["kind", "placement", "tile_kind"]);
        Feature f = Feature.create(featureconf["kind"].str, params);

        if (pos.invalid) {
            if (! ("placement" in featureconf))
                throw new Exception("Missing feature placement config for '"
                    ~ name ~ "' in " ~ filename);

            pos = findPosition(map, name, featureconf["placement"]);
        }

        map.at(pos).feature = f;

        // return the position when creating a map changer
        // This is required to get the starting point for map validation
        if (featureconf["kind"].str == "map_changer") {
            return pos;
        } else {
            return Position();
        }
    }

    Position findPosition(Map map, string name, TOMLValue posconfig) {
        Position pos;

        switch (posconfig["method"].str) {
            case "adjacent":
                pos = map.findSpaceAdjacentToKind(
                    posconfig["adjacent_to"].str,
                    ("adjacent_count" in posconfig) ? to!int(posconfig["adjacent_count"].integer) : 3
                    );
                break;

            case "anywhere":
                pos = map.findSpace();
                break;

            case "dead end":
                pos = map.findSpace(Yes.seekDeadEnd);
                break;

            default:
                break;
        }
        if (pos.invalid)
            throw new Exception("Could not find a position for '"
                ~ name ~ "' in " ~ filename);
        logf(LogLevel.info, ", position: %s", pos.toString);
        return pos;
    }

    private string[string] getParams(TOMLValue config, string[] exclude=[]) {
        string[string] params;

        foreach (key, value; config) {
            if (canFind(exclude, key))
                continue;

            params[key] = value.str;
        }
        return params;
    }
}
