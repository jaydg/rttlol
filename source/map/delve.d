/* Written by Kusigrosz in December 2008, April 2011
 * This program is in public domain, with all its bugs etc.
 * No warranty whatsoever.
 *
 * Delve a random maze/cavern. The results are written as text to stdout
 * and as a bmp file to BMP_OUTFILE (see the #defines below the comment),
 *
 * Usage:
 * default:
 *     delve_dgp name pullflag storeflag cellnum
 * alternative:
 *     delve_dgp pullflag storeflag cellnum
 *
 * Arguments:
 *     name: the name of the string describing the digperm array, which
 *         holds probabilities of digging being permitted for different
 *         neighbour patterns. If the name is not found, it is assumed
 *         the program is invoked in the alternative form, when the
 *         digperm table is generated randomly.
 *     pullflag: defines how next cells to be considered for digging
 *         are selected from the store; see below for details.
 *     storeflag: defines how neighbouring cells are stored; see below
 *         for details.
 *     cellnum: the maximum number of floor cells that will be generated.
 *
 * The default values of the arguments are #defined below this comment.
 *
 * Algorithm description:
 * The algorithm operates on a rectangular grid. Each cell can be WALL
 * or FLOOR. A (non-border) cell has 8 neigbours - diagonals count.
 * There is also a cell store with two operations: store a given cell
 * on top, and pull a cell from the store. There is no check for
 * repetitions, so a given cell can be stored multiple times.
 * The cell to be pulled is selected depending on the value of pullflag:
 *     CUBEROOT: randomly from all the store if N_in_store < 125,
 *         and randomly from the top 25 * cube_root(N_in_store) otherwise.
 *     ALL: randomly from all the store.
 *     BOTTOM: always the bottommost cell of the store.
 * The algorithm starts with most of the map filled with WALL, with a
 * "seed" of some FLOOR cells; their neigbouring WALL cells are in
 * store. The main loop in delveon() is repeated until the desired
 * number of FLOOR cells is reached, or there is nothing in store:
 *     Pull a cell from the store;
 *     Encode the WALL/FLOOR pattern of its 8 neighbours in an 8-bit
 *         number, and use it as an index into the digperm table, to
 *         fetch the permil probability of such pattern permitting digging.
 *     With the given probability, convert the cell to FLOOR, and store
 *         its WALL neighbours, depending on the value of the storeflag:
 *         PERM: in random order
 *         CW: clockwise (starting from a random one)
 *         CCW: counterclockwise (starting from a random one)
 *
 * The digperm table entries are 256 values from 0 to 1000; the bits
 * of the indices correspond to the neighbours of the given cell - lsb
 * is the cell to the right, then clockwise; FLOOR is 1 and WALL 0.
 * The values are permil probabilities of the cell being allowed for
 * digging. For example, an entry 750 at index 179 (10110011) means
 * that the pattern:
 *  101
 *  1c1
 *  001
 * will permit digging of the central cell with probability 0.75
 *
 * If the name of the digperm string is not found, the digperm table
 * is generated randomly in randdigperm. As such random generation
 * may result in a table that won't allow digging enough cells,
 * the table generation / cavern generation is repeated until a high
 * enough number of cells is dug.
 *
 * The digperm table used in generating the current pattern is dumped
 * to the file DIGPERM_OUTFILE.
 */
module map.delve;

import std.algorithm : canFind;
import std.conv : to;
import std.experimental.logger: log, logf, LogLevel;
import std.random;

import map;
import position;
import tile;

class DelveMap : Map
{
    // pullflag values: from which part of cellstore cells are pulled
    private enum Pull
    {
        CUBEROOT = 1,
        ALL = 2,
        BOTTOM = 3,
        DEFAULT = CUBEROOT
    }

    // storeflag values: in what order the neighbour cells are stored
    private enum Store
    {
        PERM = 1,
        CW = 2,
        CCW = 3,
        DEFAULT = PERM
    }

    private class Cellstore
    {
        private int[] xs;
        private int[] ys;
        private int index;
        private int size;

        // Integer cube root approximation (from below) for positive ints < 2**31
        private int uti_icbrt(int arg)
        {
            int delta;
            int ret;

            assert((arg >= 0) && (arg <= 0x7fffffff));

            if (arg == 0) {
                return (0);
            }

            if (arg < 1000) {
                ret = 10;
            } else if (arg < 1_000_000) {
                ret = 100;
            } else if (arg < 1_000_000_000) {
                ret = 1000;
            } else {
                // the largest x for which x**3 < 2**31
                ret = 1290;
            }

            do {
                // derivative = 3 * x * x yadda yadda, but this seems to works OK
                delta = (arg - ret * ret * ret) / (2 * ret * ret);
                ret += delta;
            } while (delta);

            if (ret * ret * ret > arg) {
                ret--;
            }

            return ret;
        }

        this(uint _size)
        {
            size = _size;
            index = 0;

            xs = new int[size];
            ys = new int[size];
        }

        bool storecell(int x, int y)
        {
            if (index < size)
            {
                xs[index] = x;
                ys[index] = y;
                index++;
                return true; /* new cell stored */
            }
            else /* Replace another cell. Should not happen if lossless storage */
            {
                int rind = uniform(0, index);
                xs[rind] = x;
                ys[rind] = y;
                return false; /* old cell gone, new cell stored */
            }
        }

        // Remove a cell from the store and put its coords in *x, *y.
        // Selection of the cell to be removed depends on the pullflag.
        // NOTE: pulling any cell except the topmost one puts the topmost one
        // in its place.
        bool rndpull(int* x, int* y, Pull pullflag)
        {
            int rind;

            if (index <= 0) {
                // no cells
                return false;
            }

            switch (pullflag)
            {
            case Pull.CUBEROOT: /* fluffy patterns */
                rind = (index < 125) ? uniform(0, index)
                    : index - uniform(0, 25 * uti_icbrt(index)) - 1;
                break;
            case Pull.ALL: /* compact patterns */
                rind = uniform(0, index);
                break;
            case Pull.BOTTOM: /* usually winding patterns */
                rind = 0;
                break;
            default:
                assert(0); /* unimplemented */
            }

            *x = xs[rind];
            *y = ys[rind];

            if (index - 1 != rind)  {
            // not the topmost cell - overwrite
                xs[rind] = xs[index - 1];
                ys[rind] = ys[index - 1];
            }

            index -= 1;

            return true;
        }
    }

    /* A table of neighbour patterns and their digperm indices is
    * presented below; '@' represents FLOOR and '-' WALL. A usable
    * digperm string like those above needs to contain only entries
    * with nonzero probabilities - others can be omitted.
    *
    * When creating a digperm table, the probabilities in entries
    * corresponding to the given pattern rotated by 90, 180 and 270
    * degrees are set (by setsymmetr()) to the same value - otherwise
    * the generated cavern would 'lean' in one direction.

    #---   ---   ---   ---   ---   ---   ---   ---   ---   ---   @--
    #-c-   -c@   -c-   -c@   -c@   -c-   -c@   -c-   @c@   @c-   -c-
    #---   ---   --@   --@   -@-   -@@   @--   @-@   ---   --@   --@
    0:0   1:0   2:0   3:0   5:0   6:0   9:0   10:0  17:0  18:0  34:0

    #---   ---   ---   ---   ---   ---   ---   ---   ---   @--   @--
    #-c@   -c@   -c@   -c-   @c@   @c@   @c-   @c@   @c-   -c@   -c@
    #-@@   @-@   @@-   @@@   --@   -@-   -@@   @--   @-@   --@   -@-
    7:0   11:0  13:0  14:0  19:0  21:0  22:0  25:0  26:0  35:0  37:0

    #@--   @--   @--   ---   ---   ---   ---   ---   @--   @--   @--
    #-c-   -c@   -c-   -c@   @c@   @c@   @c@   @c-   -c@   -c@   -c@
    #-@@   @--   @-@   @@@   -@@   @-@   @@-   @@@   -@@   @-@   @@-
    38:0  41:0  42:0  15:0  23:0  27:0  29:0  30:0  39:0  43:0  45:0

    #@--   @--   @--   @--   @--   @--   -@-   -@-   -@-   @@-   @@-
    #-c-   @c@   @c@   @c-   @c@   @c-   @c@   @c-   @c-   -c-   -c-
    #@@@   --@   -@-   -@@   @--   @-@   -@-   -@@   @-@   -@@   @-@
    46:0  51:0  53:0  54:0  57:0  58:0  85:0  86:0  90:0  102:0 106:0

    #@-@   ---   @--   @--   @--   @--   @--   -@-   -@-   -@-   @@-
    #-c-   @c@   -c@   @c@   @c@   @c@   @c-   @c@   @c@   @c-   -c@
    #@-@   @@@   @@@   -@@   @-@   @@-   @@@   -@@   @-@   @@@   -@@
    170:0 31:0  47:0  55:0  59:0  61:0  62:0  87:0  91:0  94:0  103:0

    #@@-   @@-   @@-   @-@   @--   -@-   @@-   @@-   @@-   @@-   @-@
    #-c@   -c-   @c-   -c@   @c@   @c@   -c@   @c@   @c@   @c-   -c@
    #@-@   @@@   @-@   @-@   @@@   @@@   @@@   -@@   @-@   @@@   @@@
    107:0 110:0 122:0 171:0 63:0  95:0  111:0 119:0 123:0 126:0 175:0

    #@-@   @@-   @-@   @@@
    #@c@   @c@   @c@   @c@
    #@-@   @@@   @@@   @@@
    187:0 127:0 191:0 255:0
    */

    private class Digperm
    {
        private int[256] digperm;

        // The Hamming weights (numbers of 1's) of values 0 - 255
        private static const int[] hamming = [
        /******  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
        /* 00 */ 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
        /* 10 */ 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
        /* 20 */ 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
        /* 30 */ 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        /* 40 */ 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
        /* 50 */ 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        /* 60 */ 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        /* 70 */ 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
        /* 80 */ 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
        /* 90 */ 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        /* a0 */ 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        /* b0 */ 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
        /* c0 */ 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
        /* d0 */ 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
        /* e0 */ 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
        /* f0 */ 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8
        ];

        // The number of groups of 1's in the 8 cell neighbourhood of a cell.
        // The lsb corresponds to the cell to the right, then clockwise.
        // A group is a set of cells that are connected (diagonally too).
        // For example:
        // 101                 110              101
        // 1x0 2 groups        1x1 1 group      0x0 4 groups
        // 010                 010              101
        private static const int[] ngb_grouptab = [
        /******* 0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
        /* 00 */ 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1,
        /* 10 */ 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1,
        /* 20 */ 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 2, 2, 2, 2,
        /* 30 */ 1, 2, 2, 2, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1,
        /* 40 */ 1, 1, 2, 1, 2, 1, 2, 1, 2, 2, 3, 2, 2, 1, 2, 1,
        /* 50 */ 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1,
        /* 60 */ 1, 1, 2, 1, 2, 1, 2, 1, 2, 2, 3, 2, 2, 1, 2, 1,
        /* 70 */ 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1,
        /* 80 */ 1, 1, 2, 1, 2, 1, 2, 1, 2, 2, 3, 2, 2, 1, 2, 1,
        /* 90 */ 2, 2, 3, 2, 2, 1, 2, 1, 2, 2, 3, 2, 2, 1, 2, 1,
        /* a0 */ 2, 2, 3, 2, 3, 2, 3, 2, 3, 3, 4, 3, 3, 2, 3, 2,
        /* b0 */ 2, 2, 3, 2, 2, 1, 2, 1, 2, 2, 3, 2, 2, 1, 2, 1,
        /* c0 */ 1, 1, 2, 1, 2, 1, 2, 1, 2, 2, 3, 2, 2, 1, 2, 1,
        /* d0 */ 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1,
        /* e0 */ 1, 1, 2, 1, 2, 1, 2, 1, 2, 2, 3, 2, 2, 1, 2, 1,
        /* f0 */ 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1
        ];

        private static enum digperm_strings = [
            // rect maze with loops
            "rmaze1": "1:1000 3:1000 6:1000 17:1000 14:0 19:1000 25:1000 27:1000 29:0 "
                ~ "51:1000 57:1000 102:1000 59:1000 107:0 110:1000 187:1000 127:0 255:0",
            // dense rect maze / cavern with lots of pillars (griddy - town)
            "rmaze2": "1:1000 17:1000 14:1000 19:1000 25:1000 27:1000 29:0 51:1000 "
                ~ "57:1000 102:1000 59:1000 107:0 110:1000 187:1000 127:0 255:0",
            // dense rect maze / cavern, long multipillars (no pillars?)
            "rmaze3": "1:1000 17:1000 14:1000 30:0 57:1000 106:0 122:0 255:0",
            // dense rect maze with 2x2 rooms at ends, may die young
            "rmaze4": "1:1000 17:1000 7:1000 14:1000 30:0 57:1000 106:0 122:0 255:0",
            // together with pulling always the 0-th cell, a rect town wallbuilder
            "rmaze5": "1:1000 6:1000 17:1000 19:1000 25:1000 27:1000 51:1000 57:1000 "
                ~ "102:1000",
            // sparse rect town grid, few pillars, may die young
            "rmaze6": "1:1000 4:1000 14:50 16:1000 17:1000 19:1000 25:1000 27:1000 "
                ~ "49:1000 1:1000 56:50 57:1000 64:1000 68:1000 70:1000 76:1000 78:1000 "
                ~ "100:1000 102:1000 108:1000 131:50 145:1000 147:1000 153:1000 177:1000 "
                ~ "196:1000 198:1000 204:1000 224:50 228:1000",
            // mostly rect maze - mostly caverns actually
            "rmaze7": "1:1000 3:1000 6:1000 5:1000 9:1000 10:1000 17:1000 18:1000 "
                ~ "34:1000 7:1000 15:1000 23:1000 29:1000 30:1000 31:1000 55:1000 "
                ~ "61:1000 62:1000 87:1000 94:1000 103:1000 63:1000 95:1000 111:1000 "
                ~ "119:1000 123:1000 126:1000 127:1000 191:1000 255:1000",
            // another rect maze
            "rmaze8": "1:1000 3:200 6:200 17:1000 14:200 19:1000 21:1000 25:1000 "
                ~ "27:200 51:200 53:1000 54:1000 57:1000 85:1000 86:1000 102:200 ",
            // rect maze, call with CW or CCW and BOTTOM
            "rmaze9": "1:1000 3:1000 5:1000 7:200 14:200 18:1000 19:200 38:1000 "
                ~ "94:200 ",

            // narrow diag maze with occasional straight connections
            "dmaze1": "2:1000 17:1000",
            // mostly diag narrow mesh */
            "dmaze2": "1:100 2:1000 9:1000 10:1000 17:1000 18:1000 34:1000",
            // mostly diag maze with some straight corridors
            "dmaze3": "2:1000 9:1000 10:1000 17:1000 18:1000 21:1000 34:1000",
            // narrow diag maze with wider caverns
            "dmaze4": "2:1000 10:1000 21:1000 85:1000 170:1000 31:1000 47:1000 "
                ~ "55:1000 59:1000 61:1000 62:1000 87:1000 91:1000 94:1000 103:1000 "
                ~ "107:1000 110:1000 122:1000 171:1000 63:1000 95:1000 111:1000 119:1000 "
                ~ "123:1000 126:1000 175:1000 187:1000 127:1000 191:1000 255:1000",
            // mixed narrow maze, try with BOTTOM
            "dmaze5": "2:1000 5:1000 37:200 53:200 58:1000 86:200 95:1000 122:200 "
                ~ "171:1000",
        ];

        // Rotate left an 8-bit value.
        private static int ROTL_8(int d, int n)
        {
            return ((d >> (8 - n)) & 0xff) | ((d << n) & 0xff);
        }

        static string[] presets()
        {
            return digperm_strings.keys;
        }

        // Fill the digperm array with permil probabilities. If desc is non-null,
        // the entries in format idx:prob are scanned from the desc string,
        // otherwise they are generated depending on ngb_min, ngb_max and conmil
        // in the following way: If the number of 1s (floor neighbours) in the
        // index is less than ngb_min or more than ngb_max, the entry stays 0.
        // Otherwise, if digging the cell c wouldn't connect previously
        // unconnected groups of floor cells, the entry is set to 1000. Otherwise,
        // the entry is set to conmil.
        this(int ngb_min, int ngb_max, int conmil, string preset)
        {
            import std.ascii : isDigit, isWhite;
            import std.stdio : sscanf;

            assert(preset in digperm_strings);

            for (int i = 0; i < digperm.length; i++) {
                digperm[i] = 0;
            }

            if (preset) {
                // scan the description, ignore other params
                string def = digperm_strings[preset];
                for (int i = 0; i < def.length; i++) {
                    int idx, prob;

                    if (isDigit(def[i]) && ((0 == i) || isWhite(def[i - 1]))
                            && (2 == sscanf(&(def[i]), "%d:%d", &idx, &prob))
                            && (idx > 0) && (idx < 256) && (prob >= 0) && (prob <= 1000)) {
                        setsymmetr(idx, prob);
                    }
                }
            } else {
                // generate from params
                assert((ngb_min >= 0) && (ngb_min <= 3) && (ngb_min <= ngb_max) && (ngb_max <= 8));
                assert((conmil >= 0) && (conmil <= 1000));

                for (int i = 0; i < hamming.length; i++) {
                    if ((hamming[i] >= ngb_min) && (hamming[i] <= ngb_max)) {
                        digperm[i] = (1 == ngb_grouptab[i]) ? 1000 : conmil;
                    }
                }
            }
        }

        // A crude way of generating a random digperm table. Will often give
        // interesting results, but certainly doesn't explore a significant part
        // of the possibilities.
        this()
        {
            int c1000, c200;
            // These are indices of patterns that are the main ways of pattern
            // expansion. If the probabilities at all these entries are too
            // low, the pattern will very likely die young.
            int[] expand = [1, 2, 3, 6, 14];

            for (int i = 0; i < digperm.length; i++) {
                digperm[i] = 0;
            }

            // Make sure the pattern has at least some possibility of expanding
            setsymmetr(cast(int) uniform(0, expand.length), 1000);

            // usually a few, sometimes a few dozen entries with 1000 permil prob
            c1000 = uniform(0, 5) + (0 == uniform(0, 4)) ? uniform(0, 30) : 4;
            for (int i = 0; i < c1000; i++) {
                setsymmetr(uniform(0, 256), 1000);
            }

            // usually a few, sometimes a few dozen entries with 200 permil prob
            c200 = uniform(0, 5) + (0 == uniform(0, 4)) ? uniform(0, 30) : 4;
            for (int i = 0; i < c200; i++) {
                setsymmetr(uniform(0, 256), 200);
            }
        }

        // Select a digperm index of Hamming weight no higher than maxham.
        // Prefer those with a higher Hamming weight - among equals chose
        // with probability proportional to the value of the digperm entry.
        // If no positive probabilities found, return -1;
        int findseed(int maxham)
        {
            int ret = -1;
            int sumprob = 0;
            int maxtillnow = 0;

            for (int i = 0; i < digperm.length; i++) {
                if ((hamming[i] <= maxham) && (digperm[i] > 0)) {
                    if (hamming[i] > maxtillnow) {
                        sumprob = digperm[i];
                        maxtillnow = hamming[i];
                    } else if (hamming[i] == maxtillnow) {
                        sumprob += digperm[i];
                    } else {
                        continue;
                    }

                    if (uniform(0, sumprob) < digperm[i]) {
                        ret = i;
                    }
                }
            }

            return ret;
        }

        // Set the digperm entry at idx to prob, and fix the symmetry by
        // copying the value to entries at idx rotated by 2, 4, 6 bits - this
        // corresponds to pattern rotation by 90, 180, 270 degrees - to prevent
        // direction bias.
        private void setsymmetr(int idx, int prob)
        {
            assert((idx >= 0) && (idx <= 256));
            assert((prob >= 0) && (prob <= 1000));

            digperm[idx] = prob;
            for (int i = 2; i <= 6; i += 2) {
                digperm[ROTL_8(idx, i)] = prob;
            }
        }
    }

    private string solid_tile = "wall";
    private string open_tile = "floor";
    private Digperm digperms;
    private Pull pullflag = Pull.DEFAULT;
    private Store storeflag = Store.DEFAULT;

    private const int[] Xoff = [1,  1,  0, -1, -1, -1,  0,  1];
    private const int[] Yoff = [0,  1,  1,  1,  0, -1, -1, -1];

    // Is the location within the borders - with 1 cell margin
    private bool inbord(uint x, uint y) {
        if ((x == 0) || (x >= size_x - 1) ||
            (y == 0) || (y >= size_y - 1))
            {
            return false;
            }
        return true;
    }

    // Is digging the cell at x, y permitted by digperm? Randomized,
    // uses the values in digperm as permil probability.
    private int ispermitted(int x, int y, string kind)
    {
        int bitmap = 0; /* lowest bit is the cell to the right, then clockwise */

        for (int i = 0; i < 8; i++)
        {
            bitmap >>= 1;

            if (inbord(x + Xoff[i], y + Yoff[i]) && at(y + Yoff[i], x + Xoff[i]).kind == kind)
            {
                bitmap |= 0x80;
            }
        }

        return (uniform(0, 1000) < digperms.digperm[bitmap]);
    }

    private int digcell(Cellstore cstore, int x, int y)
    {
        static int[] order = [0, 1, 2, 3, 4, 5, 6, 7];

        if (!inbord(x, y) || (at(y, x).kind != solid_tile)) {
            return 0; /* did nothing */
        }

        at(y, x).kind = open_tile;

        switch (storeflag)
        {
        case Store.PERM:
            randomShuffle(order);
            break;
        case Store.CW:
            immutable r = uniform(0, 8);
            for (int i = 0; i < 8; i++) {
                order[i] = (r + i) % 8;
            }
            break;
        case Store.CCW:
            immutable r = uniform(0, 8);
            for (int i = 0; i < 8; i++) {
                order[i] = (8 + r - i) % 8;
            }
            break;
        default:
            assert(0);
        }

        for (int i = 0; i < 8; i++) {
            immutable j = order[i];
            if (inbord(x + Xoff[j], y + Yoff[j]) && (at(y + Yoff[j], x + Xoff[j]).kind == solid_tile))
            {
                cstore.storecell(x + Xoff[j], y + Yoff[j]);
            }
        }

        return 1; /* dug 1 cell */
    }

    // Continue digging until cellnum or no more cells in store.
    private int delveon(Cellstore cstore, int cellnum)
    {
        int count = 0;
        int x, y;

        assert(cellnum >= 0);

        while ((count < cellnum) && cstore.rndpull(&x, &y, pullflag)) {
            if (ispermitted(x, y, open_tile)) {
                count += digcell(cstore, x, y);
            }
        }

        return count;
    }

    // Generate a random cavern of cellnum cells.
    private int cavern(int xorig, int yorig, int cellnum)
    {
        int count = 0;

        assert(cellnum >= 0);
        assert(open_tile != solid_tile);

        int seed = digperms.findseed(cellnum);
        if (seed < 0) {
            // can't start delving.
            return count;
        }

        Cellstore cstore = new Cellstore(8 * size_x * size_y); /* overkill */

        count += digcell(cstore, xorig, yorig);

        for (int i = 0; i < 8; i++) {
            int x = xorig + Xoff[i];
            int y = yorig + Yoff[i];
            if ((count < cellnum) && ((seed >> i) & 0x1)) {
                count += digcell(cstore, x, y);
            }
        }

        if (count < cellnum) {
            count += delveon(cstore, cellnum - count);
        }

        return count;
    }

    package this(string[string] params)
    {
        foreach (string param; ["size_x", "size_y"]) {
            if (! (param in params))
                throw new Exception("Missing parameter '%s'", param);
        }

        super(to!int(params["size_x"]), to!int(params["size_y"]));

        string preset;
        uint cellnum = size_y * size_x / 5;

        if ("preset" in params) {
            if (!Digperm.presets.canFind(params["preset"])) {
                throw new Exception("Unknown preset: " ~ params["preset"]);
            } else {
                preset = params["preset"];
            }
        }

        if ("pullflag" in params) {
            try {
                pullflag = to!Pull(params["pullflag"]);
            } catch (Exception) {
                throw new Exception("Unknown pullflag:" ~ params["pullflag"]);
            }
        }

        if ("storeflag" in params) {
            try {
                storeflag = to!Store(params["storeflag"]);
            } catch (Exception) {
                throw new Exception("Unknown storeflag: " ~ params["storeflag"]);
            }
        }

        if ("dig_percent" in params) {
            uint percent;
            try {
                percent = to!int(params["dig_percent"]);
                if (percent < 1 || percent > 100) {
                    throw new Exception("message irrelevant");
                }
            } catch (Exception) {
                throw new Exception("Invalid value for dig_percent: " ~ params["dig_percent"]);
            }

            cellnum = (size_y * size_x * percent) / 100;
        }

        if ("solid_tile" in params) {
            if (!Tile.kinds.canFind(params["solid_tile"])) {
                throw new Exception("Invalid value for solid_tile: " ~ params["solid_tile"]);
            }
            solid_tile = params["solid_tile"];
        }

        if ("open_tile" in params) {
            if (!Tile.kinds.canFind(params["open_tile"])) {
                throw new Exception("Invalid value for open_tile: " ~ params["open_tile"]);
            }
            open_tile = params["open_tile"];
        }

        int dugcells;
        do {
            // obliterate any previous results
            fill(solid_tile);

            if (preset) {
                logf(LogLevel.info, "Using digperm table named: \"%s\"", preset);
                digperms = new Digperm(0, 0, 0, preset);
                dugcells = cavern(size_x / 2, size_y / 2, cellnum);
            } else {
                log(LogLevel.info, "Generating a random digperm table.");
                // try random digperms until succesful enough
                digperms = new Digperm();
                dugcells = cavern(size_x / 2, size_y / 2, cellnum);
            }
            logf("pullflag: %s", to!string(pullflag));
            logf("storeflag: %s", to!string(storeflag));
            logf("solid_tile: %s", solid_tile);
            logf("open_tile: %s", open_tile);
            logf("wanted dug cells: %d - actually dug cells: %d", cellnum, dugcells);
        } while ((dugcells < cellnum) && (dugcells < size_x * size_y / 3));
    }
}
