module map;

// allow using Flag everywhere
public import std.typecons : No, Yes;

import std.algorithm : remove;
import std.array;
import std.conv : to;
import std.experimental.logger;
import std.json;
import std.random;
import std.typecons : Flag;

import game;
import mobile.monster;
import position;
import tile;

import map.loader;
import map.spawn;

public class Map
{
    protected int _size_x;
    protected int _size_y;
    private string _name;
    private string _description;
    private int _scale;
    package uint _illumination = 10;
    package Position _starting_position;
    protected Tile[][] data;
    package Monster[] monsters;
    package Spawn spawn;

    package this(int size_x, int size_y, string kind="void") {
        _size_y = size_y;
        _size_x = size_x;

        data = new Tile[][](size_y, size_x);

        foreach (immutable y; 0 .. size_y) {
            foreach (immutable x; 0 .. size_x) {
                try {
                    data[y][x] = new Tile(kind);
                } catch (Exception e) {
                    log(LogLevel.critical, "Error: ", e.msg);
                }
            }
        }
    }

    public this(JSONValue json) {
        _size_x = cast(int)json["size_x"].integer;
        _size_y = cast(int)json["size_y"].integer;
        name = json["name"].str;
        description = json["description"].str;
        scale = cast(int)json["scale"].integer;
        _illumination = cast(uint)json["illumination"].integer;

        data = new Tile[][](size_y, size_x);

        int y;
        foreach (row; json["data"].array) {
            int x;
            foreach (cell; JSONValue(row).array) {
                data[y][x++] = new Tile(cell);
            }
            y++;
        }

        if ("spawn" in json) {
            spawn = new Spawn(json["spawn"]);
        }

        foreach (jmonster; json["monsters"].array) {
            new Monster(jmonster, this);
        }
    }

    JSONValue toJSON() {
        JSONValue[][] tiles;
        foreach(row; data) {
            JSONValue[] cells;
            foreach(cell; row) {
                cells ~= cell.toJSON();
            }

            tiles ~= cells;
        }

        JSONValue[string] json;
        json["size_x"] = JSONValue(_size_x);
        json["size_y"] = JSONValue(_size_y);
        json["name"] = JSONValue(_name);
        json["description"] = JSONValue(_description);
        json["scale"] = JSONValue(_scale);
        json["illumination"] = JSONValue(_illumination);
        json["data"] = tiles;

        if (spawn) {
            json["spawn"] = spawn.toJSON();
        }

        JSONValue[] jmonsters;
        foreach (monster; monsters) {
            jmonsters ~= monster.toJSON();
        }
        json["monsters"] = jmonsters;

        return JSONValue(json);
    }

    static public Map create(string name) {
        auto ml = new MapLoader(name);
        return ml.createMap();
    }

    @property {
        public string name() {
            return _name;
        }

        package string name(string name) {
            return _name = name;
        }

        public string description() {
            return _description;
        }

        package string description(string description) {
            return _description = description;
        }

        public int size_x() {
            return _size_x;
        }

        public int size_y() {
            return _size_y;
        }

        public Position startingPosition() {
            return _starting_position;
        }

        public int scale() {
            return _scale;
        }

        package int scale(int s) {
            return _scale = s;
        }

        public uint illumination() {
            if (_illumination == 100) {
                // outdoor map, take daylight into account
                return to!uint(_illumination * Game.get.clock.daylight);
            } else {
                return _illumination;
            }
        }
    }

    public Tile at(int y, int x) {
        return data[y][x];
    }

    public Tile at(Position pos) {
        return data[pos.y][pos.x];
    }

    public void addMonster(Monster monster) {
        monsters ~= monster;
    }

    public void removeMonster(Monster monster) {
        monsters = remove!(m => m == monster)(monsters);
    }

    public void moveMonsters() {
        foreach (m ; monsters) {
            if (m.canMove) {
                m.move();
            }
        }
    }

    public bool isValid(int y, int x) {
        return ((y >= 0 && y < this._size_y)
            && (x >= 0 && x < this._size_x));
    }

    public bool isValid(Position pos) {
        if (pos.invalid) {
            return false;
        } else {
            return isValid(pos.y, pos.x);
        }
    }

    protected void fill(string kind) {
        foreach (immutable y; 0 .. size_y) {
            foreach (immutable x; 0 .. size_x) {
                at(y, x).kind = kind;
            }
        }
    }

    protected int percentDug() {
        int size = this._size_y * this._size_x;
        int count = 0;

        foreach (immutable y; 0 .. _size_y) {
            foreach (immutable x; 0 .. _size_x) {
                if (at(y, x).isPassable()) {
                    count++;
                }
            }
        }

        return (count * 100) / size;
    }

    protected int neighboursOfKind(int y, int x, string kind) {
        int neighbours = 0;

        foreach (immutable py; y - 1 .. y + 2) {
            foreach (immutable px;  x - 1 .. x + 2) {
                if (!isValid(py, px)) {
                    continue;
                }

                if (at(py, px).kind() == kind) {
                    neighbours++;
                }
            }
        }

        return neighbours;
    }

    public override string toString() {
        string str = "";

        foreach (immutable y; 0 .. _size_y) {
            foreach (immutable x; 0 .. _size_x) {
                str ~= data[y][x].glyph();
            }
            str ~='\n';
        }

        return str;
    }

    protected bool isDeadEnd(int y, int x)
    {
        // check for an dead end
        // useful in maze maps
        int blocked_count = 0;

        foreach (immutable py; y - 1 .. y + 2) {
            foreach (immutable px; x - 1 ..  x + 2) {
                if (!isValid(py, px)) continue;
                if (!at(py, px).isPassable()) blocked_count++;
            }
        }

        return (blocked_count == 7);
    }

    public Position findSpace(Flag!"seekDeadEnd" seekDeadEnd=No.seekDeadEnd) {
        int count, iteration = 0;

        int x = uniform(0, this._size_x - 1);
        int y = uniform(0, this._size_y - 1);

        // number of positions inside the map
        count = this._size_x * this._size_y;

        do {
            x++;

            if (x == this._size_x) {
                x = 0;
                y++;
            }
            if (y == this._size_y) y = 0;

            // check for dead end if requested
            bool ok = (seekDeadEnd) ? isDeadEnd(y, x) : true;
            Tile t = this.at(y, x);
            if (t.isPassable() && (! t.feature) && ok) break;

            iteration++;
        }
        while ((iteration <= count));

        // return null if no position could be found
        return (iteration > count ) ? Position() : Position(x, y);
    }

    public Position findSpaceAdjacentToKind(string kind, int count=3) {
        int x = uniform(0, size_x - 1);
        int y = uniform(0, size_y - 1);
        int tilecount = size_x * size_y;
        int iteration = 0;

        while(iteration < tilecount) {
            if (++x == size_x) {
                x = 0;
                y++;
            }
            if (y == size_y) {
                y = 0;
            }

            Tile t = this.at(y, x);
            if (t.isPassable && (! t.feature) && this.neighboursOfKind(y, x, kind) >= count)
                break;

            iteration++;
        }

        return (iteration == tilecount) ? Position() : Position(x, y);
    }

    public Position findExit(string to)
    in {
        assert(to !is null);
    } body {
        import feature.map_changer;

        foreach (immutable y; 0 .. size_y) {
            foreach (immutable x; 0 .. size_x) {
                if (at(y, x).feature) {
                    auto mc = cast(MapChanger)at(y, x).feature;
                    if (mc !is null && mc.destination == to)
                        return Position(x, y);
                }
            }
        }

        // if we reach this point, the exit is not on the map
        return Position();
    }

    public string[] getSurrounding(Position pos, Tile.Element elementType)
    {
        import mobile;
        import feature;

        string[] dirs;

        foreach (direction; Position.directions) {
            Position p = Position(pos).move(direction);

            if (!isValid(p))
                continue;

            // looking for a mob?
            if (elementType == Tile.Element.MOBILE) {
                if (at(p).mobile) {
                    dirs ~= direction;
                    continue;
                }
            }

            // looking for a feature?
            if (elementType == Tile.Element.FEATURE) {
                if (at(p).feature)
                    dirs ~= direction;
            }
        }

        return dirs;
    }

    package bool validate(Position start)
    in {
        assert(isValid(start));
    } body {
        //
        int[][] obsmap;
        obsmap.length = size_y;
        foreach (immutable y; 0 .. size_y) {
            obsmap[y].length = size_x;
            foreach (immutable x; 0 .. size_x) {
                obsmap[y][x] = cast(int)at(y, x).isPassable;
            }
        }

        Position[] stack = [start];

        while (!stack.empty) {
            Position p = stack.back;
            stack.popBack;

            if (isValid(p) && obsmap[p.y][p.x] == 1) {
                obsmap[p.y][p.x] = 2;
                stack.assumeSafeAppend;

                if (obsmap[p.y][p.x - 1] == 1) stack ~= Position(p.x - 1, p.y);
                if (obsmap[p.y][p.x + 1] == 1) stack ~= Position(p.x + 1, p.y);
                if (obsmap[p.y - 1][p.x] == 1) stack ~= Position(p.x, p.y - 1);
                if (obsmap[p.y + 1][p.x] == 1) stack ~= Position(p.x, p.y + 1);
                if (obsmap[p.y - 1][p.x - 1] == 1) stack ~= Position(p.x - 1, p.y - 1);
                if (obsmap[p.y + 1][p.x + 1] == 1) stack ~= Position(p.x + 1, p.y + 1);
                if (obsmap[p.y - 1][p.x + 1] == 1) stack ~= Position(p.x + 1, p.y - 1);
                if (obsmap[p.y + 1][p.x - 1] == 1) stack ~= Position(p.x - 1, p.y + 1);
            }
        }

        // ensure all positions were touched
        foreach (immutable y; 0 .. size_y) {
            foreach (immutable x; 0 .. size_x) {
                if (obsmap[y][x] == 1) {
                    logf(LogLevel.trace, "Failed to validate map (failure at y: %d, x: %d)", y, x);
                    return false;
                }
            }
        }
        return true;
    }
}
