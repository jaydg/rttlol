module map.spawn;

import std.experimental.logger: logf, LogLevel;
import std.format;
import std.json;
import std.random;
import toml;

import map;
import mobile.monster;
import position;

class Spawn {
    // Monster spawn probability:
    //
    // AD&D: Very rare indicates a 4% chance of occurrence,
    //       rare indicates an 11% chance,
    //       uncommon indicates a 20% chance,
    //       and common indicates a 65% chance.
    private enum probabilities = [
        "very rare": 4,
        "rare":     11,
        "uncommon": 20,
        "common":   65
    ];

    private long min_count;
    private long max_count;
    private bool respawn;
    private long[string] random_monsters;

    package this(TOMLValue config) {
        if ("min_count" in config) {
            min_count = config["min_count"].integer;
        }

        if ("max_count" in config) {
            max_count = config["max_count"].integer;
        }

        if ("respawn" in config) {
            respawn = (config["respawn"].type == true);
        }

        if ("random" in config) {
            foreach (kind, probability; config["random"]) {
                if (! (probability.str in probabilities)) {
                    throw new Exception(format("Probability '%s' assigned to '%s' is not defined",
                            probability, kind));
                }
                random_monsters[kind] = probabilities[probability.str];
            }
        }
    }

    package this(JSONValue ser) {
        min_count = ser["min_count"].integer;
        max_count = ser["max_count"].integer;
        respawn = ser["respawn"].type() == JSONType.true_;
        foreach(name, value; ser["random_monsters"].object) {
            random_monsters[name] = value.integer;
        }
    }

    package JSONValue toJSON() {
        JSONValue json;

        json["min_count"] = JSONValue(min_count);
        json["max_count"] = JSONValue(max_count);
        json["respawn"] = JSONValue(respawn);
        json["random_monsters"] = JSONValue(random_monsters);

        return JSONValue(json);
    }

    public void spawn(Map map) {
        long newMonsterCount = uniform(min_count, max_count);
        if (newMonsterCount <= map.monsters.length) {
            return;
        }

        logf(LogLevel.info, "SPAWN: will create %d monsters for map '%s'.",
            newMonsterCount - map.monsters.length, map.name);

        while (map.monsters.length < newMonsterCount) {
            string newMonsterKind;

            // select new monster
            do {
                foreach (kind, probability; random_monsters) {
                    if (uniform(0, 100) <= probability) {
                        newMonsterKind = kind;
                        break;
                    }
                }
            } while (newMonsterKind == null);

            // place new monster
            Position pos;
            do {
                pos = map.findSpace();
            } while (map.at(pos).mobile !is null);

            Monster newMonster = new Monster(newMonsterKind, map, pos);
            logf(LogLevel.info, "\tGenerated a '%s' (%d) at %s", newMonster.name,
                newMonster.id, pos.toString());
        }
    }
}
