import std.experimental.logger: log, logf, LogLevel;
import std.json;
import std.string;
import toml;

import drawable;
import feature;
import mobile;

class Tile : Drawable
{
    private struct TileData {
        char glyph;
        string color;
        string description;
        bool passable;
        bool transparent;
        size_t movementCost;
    }

    private string _kind;
    private Feature _feature;
    private Mobile _mobile;

    private static TileData[string] data;

    public enum Element {
        FEATURE,
        MOBILE
    }

    static this() {
        // Load the tile data
        log(LogLevel.info, "Loading tile data.");
        import std.file : read;

        try {
            auto tileconfig = parseTOML(cast(string)read("data/tiles.conf"));

            // Find all tile type definitions
            foreach (ttd; tileconfig.keys) {
                log(LogLevel.info, "Found tile type data: ", ttd);
                TileData td = TileData();
                td.glyph       = tileconfig[ttd]["glyph"].str[0];
                log(LogLevel.info, "\tglyph:       ", td.glyph);
                td.color       = tileconfig[ttd]["color"].str;
                log(LogLevel.info, "\tcolor:       ", td.color);
                td.description = tileconfig[ttd]["description"].str;
                log(LogLevel.info, "\tdescription: ", td.description);
                td.passable    = (tileconfig[ttd]["passable"] == true);
                log(LogLevel.info, "\tpassable:    ", td.passable);
                td.transparent = (tileconfig[ttd]["transparent"] == true);
                log(LogLevel.info, "\ttransparent: ", td.transparent);

                if ("movement_cost" in tileconfig[ttd]) {
                    td.movementCost = cast(size_t)tileconfig[ttd]["movement_cost"].integer;
                    log(LogLevel.info, "\tmovement_cost: ", td.movementCost);
                }

                data[ttd] = td;
            }
        } catch (Exception e) {
            log(LogLevel.critical, "Error: ", e.msg);
        }

        // Optimize the array before usage
        data.rehash;
    }

    public static string[] kinds() {
        return data.keys;
    }

    JSONValue toJSON() {
        JSONValue[string] json;
        json["k"] = JSONValue(_kind);

        if (_feature)
            json["f"] = _feature.toJSON;

        return JSONValue(json);
    }

    public this(string kind) {
        this.kind = kind;
    }

    public this(JSONValue json) {
        _kind = json["k"].str;

        if ("f" in json) {
            _feature = Feature.fromJSON(json["f"]);
        }
    }

    @property {
        public string kind() {
            return _kind;
        }

        public string kind(string kind) {
            if (! (kind in data)) {
                throw new Exception(format("The tile kind '%s' is not known!", kind));
            }

            return _kind = kind;
        }

        public Feature feature() {
            return _feature;
        }

        public Mobile mobile () {
            return _mobile;
        }

        public Feature feature(Feature f)
        in {
            assert ( (f !is null && _feature is null)
                || (f is null && _feature !is null) );
        } body {
            return _feature = f;
        }

        public Mobile mobile(Mobile mob)
        in {
            // either there is no mob currently on the tile and a new is being
            // placed or there is one on the tile that is being removed
            assert ( (mob !is null && _mobile is null)
                || (mob is null && _mobile !is null) );
        } body {
            return _mobile = mob;
        }
    } // @property

    public string description() {
        return data[_kind].description;
    }

    public string examine() {
        string description = capitalize(this.description) ~ "."
            ~ (this.mobile ? " " ~ capitalize(this.mobile.name) ~ " ("~ this.mobile.description ~ ")." : "")
            ~ (this.feature ? " " ~ capitalize(this.feature.description) ~ "." : "");

        return description;
    }

    public bool isPassable() {
        return data[_kind].passable && (_feature is null || _feature.passable) &&(_mobile is null);
    }

    public bool isTransparent() {
        return data[_kind].transparent && (_feature is null || _feature.transparent);
    }

    // from Drawable interface
    public char glyph() {
        if (_mobile)  return _mobile.glyph;
        if (_feature) return _feature.glyph;
        return data[_kind].glyph;
    }

    public string color() {
        if (_mobile)  return _mobile.color;
        if (_feature) return _feature.color;
        return data[_kind].color;
    }

    public size_t movementCost() {
        return data[_kind].movementCost;
    }
}
