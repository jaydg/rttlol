module display.screen;

version (Posix) {
    import deimos.ncurses;
}

version (Windows) {
    import pdcurses;
}

import core.stdc.locale : LC_CTYPE, setlocale;

class Screen
{
    private static bool _initialized;
    private static size_t[string] colors;

    @property {
        public static bool initialized()
        {
            return _initialized;
        }

        package static size_t color(string name)
        {
            return colors[name];
        }
    }

    public static enum directionKeys = [
        'h': "w",
        '4': "w",
        KEY_LEFT: "w",

        'y': "nw",
        '7': "nw",
        KEY_HOME: "nw",

        'l': "e",
        '6': "e",
        KEY_RIGHT: "e",

        'n': "se",
        '3': "se",
        KEY_NPAGE: "se",

        'k': "n",
        '8': "n",
        KEY_UP: "n",

        'u': "ne",
        '9': "ne",
        KEY_PPAGE: "ne",

        'j' : "s",
        '2': "s",
        KEY_DOWN:"s",

        'b': "sw",
        '1': "sw",
        KEY_END: "sw"
    ];

    public static init()
    {
        // empty locale to ensure upper-plane glyphs are availabel
        setlocale(LC_CTYPE,"");

        version (Posix) {
            ESCDELAY = 0;    // dont wait if reading an ESC key
        }
        initscr();           // initialize curses
        raw();               // control special keys in application
        noecho();            // supress input echo
        curs_set(0);         // make the cursor invisible
        stdscr.keypad(true); // enable function keys
        stdscr.meta(true);   // want all 8 bits
        start_color();       // initialize color mode

        // initialize all the fancy colors
        foreach(ushort i; 0 .. 256) {
            init_pair(i, i, 0);
        }

        // prepare a hash table of all available colors
        colors["000000"] = COLOR_PAIR(0);   // black
        colors["800000"] = COLOR_PAIR(1);   // red
        colors["008000"] = COLOR_PAIR(2);   // green
        colors["808000"] = COLOR_PAIR(3);   // brown
        colors["000080"] = COLOR_PAIR(4);   // blue
        colors["800080"] = COLOR_PAIR(5);   // magenta
        colors["008080"] = COLOR_PAIR(6);   // cyan
        colors["c0c0c0"] = COLOR_PAIR(7);   // light gray
        colors["808080"] = COLOR_PAIR(8);   // dark gray
        colors["ff0000"] = COLOR_PAIR(9);   // light red
        colors["00ff00"] = COLOR_PAIR(10);  // light green
        colors["ffff00"] = COLOR_PAIR(11);  // yellow
        colors["0000ff"] = COLOR_PAIR(12);  // light blue
        colors["ff00ff"] = COLOR_PAIR(13);  // light magenta
        colors["00ffff"] = COLOR_PAIR(14);  // light cyan
        colors["ffffff"] = COLOR_PAIR(15);  // white
        //colors["000000"] = COLOR_PAIR(16);
        colors["00005f"] = COLOR_PAIR(17);
        colors["000087"] = COLOR_PAIR(18);
        colors["0000af"] = COLOR_PAIR(19);
        colors["0000d7"] = COLOR_PAIR(20);
        //colors["0000ff"] = COLOR_PAIR(21);
        colors["005f00"] = COLOR_PAIR(22);
        colors["005f5f"] = COLOR_PAIR(23);
        colors["005f87"] = COLOR_PAIR(24);
        colors["005faf"] = COLOR_PAIR(25);
        colors["005fd7"] = COLOR_PAIR(26);
        colors["005fff"] = COLOR_PAIR(27);
        colors["008700"] = COLOR_PAIR(28);
        colors["00875f"] = COLOR_PAIR(29);
        colors["008787"] = COLOR_PAIR(30);
        colors["0087af"] = COLOR_PAIR(31);
        colors["0087d7"] = COLOR_PAIR(32);
        colors["0087ff"] = COLOR_PAIR(33);
        colors["00af00"] = COLOR_PAIR(34);
        colors["00af5f"] = COLOR_PAIR(35);
        colors["00af87"] = COLOR_PAIR(36);
        colors["00afaf"] = COLOR_PAIR(37);
        colors["00afd7"] = COLOR_PAIR(38);
        colors["00afff"] = COLOR_PAIR(39);
        colors["00d700"] = COLOR_PAIR(40);
        colors["00d75f"] = COLOR_PAIR(41);
        colors["00d787"] = COLOR_PAIR(42);
        colors["00d7af"] = COLOR_PAIR(43);
        colors["00d7d7"] = COLOR_PAIR(44);
        colors["00d7ff"] = COLOR_PAIR(45);
        //colors["00ff00"] = COLOR_PAIR(46);
        colors["00ff5f"] = COLOR_PAIR(47);
        colors["00ff87"] = COLOR_PAIR(48);
        colors["00ffaf"] = COLOR_PAIR(49);
        colors["00ffd7"] = COLOR_PAIR(50);
        //colors["00ffff"] = COLOR_PAIR(51);
        colors["5f0000"] = COLOR_PAIR(52);
        colors["5f005f"] = COLOR_PAIR(53);
        colors["5f0087"] = COLOR_PAIR(54);
        colors["5f00af"] = COLOR_PAIR(55);
        colors["5f00d7"] = COLOR_PAIR(56);
        colors["5f00ff"] = COLOR_PAIR(57);
        colors["5f5f00"] = COLOR_PAIR(58);
        colors["5f5f5f"] = COLOR_PAIR(59);
        colors["5f5f87"] = COLOR_PAIR(60);
        colors["5f5faf"] = COLOR_PAIR(61);
        colors["5f5fd7"] = COLOR_PAIR(62);
        colors["5f5fff"] = COLOR_PAIR(63);
        colors["5f8700"] = COLOR_PAIR(64);
        colors["5f875f"] = COLOR_PAIR(65);
        colors["5f8787"] = COLOR_PAIR(66);
        colors["5f87af"] = COLOR_PAIR(67);
        colors["5f87d7"] = COLOR_PAIR(68);
        colors["5f87ff"] = COLOR_PAIR(69);
        colors["5faf00"] = COLOR_PAIR(70);
        colors["5faf5f"] = COLOR_PAIR(71);
        colors["5faf87"] = COLOR_PAIR(72);
        colors["5fafaf"] = COLOR_PAIR(73);
        colors["5fafd7"] = COLOR_PAIR(74);
        colors["5fafff"] = COLOR_PAIR(75);
        colors["5fd700"] = COLOR_PAIR(76);
        colors["5fd75f"] = COLOR_PAIR(77);
        colors["5fd787"] = COLOR_PAIR(78);
        colors["5fd7af"] = COLOR_PAIR(79);
        colors["5fd7d7"] = COLOR_PAIR(80);
        colors["5fd7ff"] = COLOR_PAIR(81);
        colors["5fff00"] = COLOR_PAIR(82);
        colors["5fff5f"] = COLOR_PAIR(83);
        colors["5fff87"] = COLOR_PAIR(84);
        colors["5fffaf"] = COLOR_PAIR(85);
        colors["5fffd7"] = COLOR_PAIR(86);
        colors["5fffff"] = COLOR_PAIR(87);
        colors["870000"] = COLOR_PAIR(88);
        colors["87005f"] = COLOR_PAIR(89);
        colors["870087"] = COLOR_PAIR(90);
        colors["8700af"] = COLOR_PAIR(91);
        colors["8700d7"] = COLOR_PAIR(92);
        colors["8700ff"] = COLOR_PAIR(93);
        colors["875f00"] = COLOR_PAIR(94);
        colors["875f5f"] = COLOR_PAIR(95);
        colors["875f87"] = COLOR_PAIR(96);
        colors["875faf"] = COLOR_PAIR(97);
        colors["875fd7"] = COLOR_PAIR(98);
        colors["875fff"] = COLOR_PAIR(99);
        colors["878700"] = COLOR_PAIR(100);
        colors["87875f"] = COLOR_PAIR(101);
        colors["878787"] = COLOR_PAIR(102);
        colors["8787af"] = COLOR_PAIR(103);
        colors["8787d7"] = COLOR_PAIR(104);
        colors["8787ff"] = COLOR_PAIR(105);
        colors["87af00"] = COLOR_PAIR(106);
        colors["87af5f"] = COLOR_PAIR(107);
        colors["87af87"] = COLOR_PAIR(108);
        colors["87afaf"] = COLOR_PAIR(109);
        colors["87afd7"] = COLOR_PAIR(110);
        colors["87afff"] = COLOR_PAIR(111);
        colors["87d700"] = COLOR_PAIR(112);
        colors["87d75f"] = COLOR_PAIR(113);
        colors["87d787"] = COLOR_PAIR(114);
        colors["87d7af"] = COLOR_PAIR(115);
        colors["87d7d7"] = COLOR_PAIR(116);
        colors["87d7ff"] = COLOR_PAIR(117);
        colors["87ff00"] = COLOR_PAIR(118);
        colors["87ff5f"] = COLOR_PAIR(119);
        colors["87ff87"] = COLOR_PAIR(120);
        colors["87ffaf"] = COLOR_PAIR(121);
        colors["87ffd7"] = COLOR_PAIR(122);
        colors["87ffff"] = COLOR_PAIR(123);
        colors["af0000"] = COLOR_PAIR(124);
        colors["af005f"] = COLOR_PAIR(125);
        colors["af0087"] = COLOR_PAIR(126);
        colors["af00af"] = COLOR_PAIR(127);
        colors["af00d7"] = COLOR_PAIR(128);
        colors["af00ff"] = COLOR_PAIR(129);
        colors["af5f00"] = COLOR_PAIR(130);
        colors["af5f5f"] = COLOR_PAIR(131);
        colors["af5f87"] = COLOR_PAIR(132);
        colors["af5faf"] = COLOR_PAIR(133);
        colors["af5fd7"] = COLOR_PAIR(134);
        colors["af5fff"] = COLOR_PAIR(135);
        colors["af8700"] = COLOR_PAIR(136);
        colors["af875f"] = COLOR_PAIR(137);
        colors["af8787"] = COLOR_PAIR(138);
        colors["af87af"] = COLOR_PAIR(139);
        colors["af87d7"] = COLOR_PAIR(140);
        colors["af87ff"] = COLOR_PAIR(141);
        colors["afaf00"] = COLOR_PAIR(142);
        colors["afaf5f"] = COLOR_PAIR(143);
        colors["afaf87"] = COLOR_PAIR(144);
        colors["afafaf"] = COLOR_PAIR(145);
        colors["afafd7"] = COLOR_PAIR(146);
        colors["afafff"] = COLOR_PAIR(147);
        colors["afd700"] = COLOR_PAIR(148);
        colors["afd75f"] = COLOR_PAIR(149);
        colors["afd787"] = COLOR_PAIR(150);
        colors["afd7af"] = COLOR_PAIR(151);
        colors["afd7d7"] = COLOR_PAIR(152);
        colors["afd7ff"] = COLOR_PAIR(153);
        colors["afff00"] = COLOR_PAIR(154);
        colors["afff5f"] = COLOR_PAIR(155);
        colors["afff87"] = COLOR_PAIR(156);
        colors["afffaf"] = COLOR_PAIR(157);
        colors["afffd7"] = COLOR_PAIR(158);
        colors["afffff"] = COLOR_PAIR(159);
        colors["d70000"] = COLOR_PAIR(160);
        colors["d7005f"] = COLOR_PAIR(161);
        colors["d70087"] = COLOR_PAIR(162);
        colors["d700af"] = COLOR_PAIR(163);
        colors["d700d7"] = COLOR_PAIR(164);
        colors["d700ff"] = COLOR_PAIR(165);
        colors["d75f00"] = COLOR_PAIR(166);
        colors["d75f5f"] = COLOR_PAIR(167);
        colors["d75f87"] = COLOR_PAIR(168);
        colors["d75faf"] = COLOR_PAIR(169);
        colors["d75fd7"] = COLOR_PAIR(170);
        colors["d75fff"] = COLOR_PAIR(171);
        colors["d78700"] = COLOR_PAIR(172);
        colors["d7875f"] = COLOR_PAIR(173);
        colors["d78787"] = COLOR_PAIR(174);
        colors["d787af"] = COLOR_PAIR(175);
        colors["d787d7"] = COLOR_PAIR(176);
        colors["d787ff"] = COLOR_PAIR(177);
        colors["dfaf00"] = COLOR_PAIR(178);
        colors["dfaf5f"] = COLOR_PAIR(179);
        colors["dfaf87"] = COLOR_PAIR(180);
        colors["dfafaf"] = COLOR_PAIR(181);
        colors["dfafdf"] = COLOR_PAIR(182);
        colors["dfafff"] = COLOR_PAIR(183);
        colors["dfdf00"] = COLOR_PAIR(184);
        colors["dfdf5f"] = COLOR_PAIR(185);
        colors["dfdf87"] = COLOR_PAIR(186);
        colors["dfdfaf"] = COLOR_PAIR(187);
        colors["dfdfdf"] = COLOR_PAIR(188);
        colors["dfdfff"] = COLOR_PAIR(189);
        colors["dfff00"] = COLOR_PAIR(190);
        colors["dfff5f"] = COLOR_PAIR(191);
        colors["dfff87"] = COLOR_PAIR(192);
        colors["dfffaf"] = COLOR_PAIR(193);
        colors["dfffdf"] = COLOR_PAIR(194);
        colors["dfffff"] = COLOR_PAIR(195);
        //colors["ff0000"] = COLOR_PAIR(196);
        colors["ff005f"] = COLOR_PAIR(197);
        colors["ff0087"] = COLOR_PAIR(198);
        colors["ff00af"] = COLOR_PAIR(199);
        colors["ff00df"] = COLOR_PAIR(200);
        //colors["ff00ff"] = COLOR_PAIR(201);
        colors["ff5f00"] = COLOR_PAIR(202);
        colors["ff5f5f"] = COLOR_PAIR(203);
        colors["ff5f87"] = COLOR_PAIR(204);
        colors["ff5faf"] = COLOR_PAIR(205);
        colors["ff5fdf"] = COLOR_PAIR(206);
        colors["ff5fff"] = COLOR_PAIR(207);
        colors["ff8700"] = COLOR_PAIR(208);
        colors["ff875f"] = COLOR_PAIR(209);
        colors["ff8787"] = COLOR_PAIR(210);
        colors["ff87af"] = COLOR_PAIR(211);
        colors["ff87df"] = COLOR_PAIR(212);
        colors["ff87ff"] = COLOR_PAIR(213);
        colors["ffaf00"] = COLOR_PAIR(214);
        colors["ffaf5f"] = COLOR_PAIR(215);
        colors["ffaf87"] = COLOR_PAIR(216);
        colors["ffafaf"] = COLOR_PAIR(217);
        colors["ffafdf"] = COLOR_PAIR(218);
        colors["ffafff"] = COLOR_PAIR(219);
        colors["ffdf00"] = COLOR_PAIR(220);
        colors["ffdf5f"] = COLOR_PAIR(221);
        colors["ffdf87"] = COLOR_PAIR(222);
        colors["ffdfaf"] = COLOR_PAIR(223);
        colors["ffdfdf"] = COLOR_PAIR(224);
        colors["ffdfff"] = COLOR_PAIR(225);
        //colors["ffff00"] = COLOR_PAIR(226);
        colors["ffff5f"] = COLOR_PAIR(227);
        colors["ffff87"] = COLOR_PAIR(228);
        colors["ffffaf"] = COLOR_PAIR(229);
        colors["ffffdf"] = COLOR_PAIR(230);
        //colors["ffffff"] = COLOR_PAIR(231);
        colors["080808"] = COLOR_PAIR(232);
        colors["121212"] = COLOR_PAIR(233);
        colors["1c1c1c"] = COLOR_PAIR(234);
        colors["262626"] = COLOR_PAIR(235);
        colors["303030"] = COLOR_PAIR(236);
        colors["3a3a3a"] = COLOR_PAIR(237);
        colors["444444"] = COLOR_PAIR(238);
        colors["4e4e4e"] = COLOR_PAIR(239);
        colors["585858"] = COLOR_PAIR(240);
        colors["626262"] = COLOR_PAIR(241);
        colors["6c6c6c"] = COLOR_PAIR(242);
        colors["767676"] = COLOR_PAIR(243);
        //colors["808080"] = COLOR_PAIR(244);
        colors["8a8a8a"] = COLOR_PAIR(245);
        colors["949494"] = COLOR_PAIR(246);
        colors["9e9e9e"] = COLOR_PAIR(247);
        colors["a8a8a8"] = COLOR_PAIR(248);
        colors["b2b2b2"] = COLOR_PAIR(249);
        colors["bcbcbc"] = COLOR_PAIR(250);
        colors["c6c6c6"] = COLOR_PAIR(251);
        colors["d0d0d0"] = COLOR_PAIR(252);
        colors["dadada"] = COLOR_PAIR(253);
        colors["e4e4e4"] = COLOR_PAIR(254);
        colors["eeeeee"] = COLOR_PAIR(255);

        // The duplicates above give us some room for dialogue colors
        colors["white_red"] = COLOR_PAIR(16);
        init_pair(16, 15, 1);

        colors["red_red"] = COLOR_PAIR(21);
        init_pair(21, 9, 1);

        colors["green_red"] = COLOR_PAIR(46);
        init_pair(46, 10, 1);

        colors["blue_red"] = COLOR_PAIR(51);
        init_pair(51, 12, 1);

        colors["yellow_red"] = COLOR_PAIR(196);
        init_pair(196, 11, 1);

        colors["white_red"] = COLOR_PAIR(201);
        init_pair(201, 15, 1);

        colors["black_red"] = COLOR_PAIR(226);
        init_pair(226, 0, 1);

        colors["black_white"] = COLOR_PAIR(231);
        init_pair(231, 0, 15);

        colors["red_white"] = COLOR_PAIR(244);
        init_pair(244, 1, 15);

        // Optimize array
        colors.rehash;

        _initialized = true;
    }

    public static terminate()
    {
        endwin();
        _initialized = false;
    }

    public static string getDirection(int key) {
        assert(key in directionKeys);

        return directionKeys[key];
    }
}
