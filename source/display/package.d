module display;

version (Posix) {
    import deimos.ncurses;
}

version (Windows) {
    import pdcurses;
}

public import std.typecons : No, Yes;

import std.algorithm;
import std.conv : to;
import std.string;
import std.typecons : Flag;

import display.screen;
import display.window;
import game;
import map;
import mobile.player;
import position;

class Display
{
    private static Screen screen;

    // translate game position to screen position: find 0,0
    private static Position getOrigin(Position pos, Map map) {
        int startX, startY;

        if (pos.x > stdscr.getmaxx()/2 - 1)
            startX = max(0, pos.x - stdscr.getmaxx() / 2);
        else
            startX = 0;

        if (pos.y > stdscr.getmaxy()/2 - 1)
            startY = max(0, pos.y - stdscr.getmaxy() / 2);
        else
            startY = 0;

        if (startX > map.size_x - stdscr.getmaxx())
            startX = max(0, map.size_x - stdscr.getmaxx());
        else
            startX = startX;

        if (startY > map.size_y - stdscr.getmaxy())
            startY = max(0, map.size_y - stdscr.getmaxy());
        else
            startY = startY;

        return Position(startX, startY);
    }

    public static void init() {
        screen.init();
    }

    public static void terminate() {
        screen.terminate();
    }

    public static void redraw(Game g) {
        clear();
        paint(g);
    }

    public static void paint(Game g) {
        static string previous_map;
        Player p = g.player();    // the player

        // clear the screen if the map is not the previously painted map
        if (previous_map != p.map.name)
            clear();
        previous_map = p.map.name;

        // starting and ending points to display
        Position origin = getOrigin(p.pos, p.map);
        immutable endX = min(p.map.size_x, origin.x + stdscr.getmaxx());
        immutable endY = min(p.map.size_y, origin.y + stdscr.getmaxy());

        for (int map_y = origin.y, screen_y = 0; map_y < endY; map_y++, screen_y++) {
            for (int map_x = origin.x, screen_x = 0; map_x < endX; map_x++, screen_x++) {
                size_t attr;
                char glyph;

                if (p.canSee(map_y, map_x)) {
                    attr = screen.color(p.map.at(map_y, map_x).color());
                    glyph = p.map.at(map_y, map_x).glyph;
                } else if (g.wizmode) {
                    attr = p.canSee(map_y, map_x)
                        ? screen.color(p.map.at(map_y, map_x).color())
                            : screen.color("585858");
                    glyph = p.map.at(map_y, map_x).glyph;
                } else {
                    attr = screen.color("585858");
                    glyph = p.memory[p.map.name][map_y][map_x];
                }
                attron(attr);
                mvaddch(screen_y, screen_x, glyph);
                attroff(attr);
            }
        }

        doupdate();
    }

    public static void message(string msg) {
        mvaddstr(stdscr.getmaxy(), 0, std.string.toStringz(msg));
        clrtoeol();
    }

    public static int get_key() {
        return getch();
    }

    public static Position getPosition(Position start, immutable string message, Flag!"passable" passable=No.passable, Flag!"visible" visible=Yes.visible)
    {
        Player player = Game.get().player;
        Position cursor = start;
        Position origin = getOrigin(cursor, player.map);
        Window popup;

        // ensure popup doesn't cover area of interest
        uint getPopupY(Position origin, Position pos) {
            import std.math : abs;

            uint popupY = LINES - 4;

            if (abs!int((pos.y - origin.y) - popupY) < 5) {
                popupY = 3;
            }

            return popupY;
        }

        if (!visible) {
            popup = Window.popup(3, getPopupY(origin, cursor), 0, null, message);
        }

        bool RUN = true;
        while(RUN) {
            // recreate popup while looking for visible positions
            if (visible && popup)
               popup.close;

            Display.paint(Game.get());

            // display message or description of selected position
            if (visible)
                popup = Window.popup(3, getPopupY(origin, cursor),
                                     0, null, player.map.at(cursor).examine);

            // show the position of the cursor by inverting the attributes
            mvchgat(cursor.y - origin.y, cursor.x - origin.x, 1, A_BOLD | A_STANDOUT,
                    COLOR_WHITE.to!short, null.to!(void*));

            Position npos;

            int key = get_key();
            switch (key) {
                case 27 /* ESC */:
                    cursor = Position();
                    RUN = false;
                    break;
                case 10 /* LF */, 13 /* CR */, KEY_ENTER:
                    // if a passable position has been requested check if it
                    // actually is passable. Only known positions are allowed.
                    if (passable && !player.map.at(cursor).isPassable()) {
                        if (!beep()) flash();
                        continue;
                    }
                    RUN = false;

                    break;

                case '@':
                    // bring the cursor back to the player
                    npos = player.pos;
                    break;

                default:
                    if (key in Screen.directionKeys) {
                        string direction = Screen.getDirection(key);
                        (npos = cursor).move(direction);
                    }
                    break;
            }

            if (player.map.isValid(npos)) {
                // valid new position
                if (visible && !(player.canSee(npos))) {
                    // don't move out of player's FOV when
                    //   a visible position was requested
                    npos = cursor;
                }
                cursor = npos;
            }
        }

        if (popup) popup.close;

        return cursor;
    }
}
