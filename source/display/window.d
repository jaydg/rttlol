module display.window;

import std.algorithm : canFind, max, min;
import std.ascii : toUpper;
import std.string : format, leftJustify, splitLines, toStringz, wrap;

version (Posix) {
    import deimos.ncurses;
    import deimos.ncurses.panel;
}

version (Windows) {
    import pdcurses;
	import pdcurses.panel;
}

import display;
import display.screen;
import game;
import position;
import util.history;

class Window
{
    private
    {
        uint x1;
        uint y1;
        uint width;
        uint height;
        string title;
        string caption;
        WINDOW* window;
        PANEL* panel;
    }

    private this() {};

    private this(uint _x1, uint _y1, uint _width, uint _height, immutable string _title) {
        this.x1 = _x1;
        this.y1 = _y1;
        this.width = _width;
        this.height = _height;

        this.window = newwin(height, width, y1, x1);
        this.panel = new_panel(this.window);

        /* fill window background */
        size_t attrs;
        wattron(this.window, (attrs = Screen.color("white_red")));

        for (int i = 1; i < height; i++)
            mvwprintw(this.window, i, 1, "%*s", width - 2, "".toStringz);

        wattroff(this.window, attrs);

        /* draw borders */
        wattron(this.window, (attrs = Screen.color("blue_red")));
        box(this.window, 0, 0);
        wattroff(this.window, attrs);

        /* set the window title */
        updateTitle(_title);

        /* refresh panels */
        update_panels();
    }

    public void close()
    {
        del_panel(this.panel);
        delwin(this.window);

        Display.paint(Game.get());
    }

    private void updateTitle(immutable string _title)
    {
        size_t attrs; /* curses attributes */

        if (this.title)
        {
            /* repaint line to overwrite previous title */
            wattron(this.window, (attrs = Screen.color("blue_red")));
            mvwhline(this.window, 0, 2, ACS_HLINE, this.width - 7);
            wattroff(this.window, attrs);
        }

        /* print the provided title */
        if (_title && _title.length)
        {
            /* copy the new title
            * the maximum length is determined by the window width
            * minus the space required for the left corner (3)
            * minus the space reqired for the right corner
            *       and the scroll marker (7)
            */
            this.title = _title[0 .. min(this.width - 10, _title.length)];

            wattron(this.window, (attrs = Screen.color("white_red")));
            mvwprintw(this.window, 0, 2, " %s ", this.title.toStringz);
            wattroff(this.window, attrs);
        }

        wrefresh(this.window);
    }

    private void updateArrowDown(bool on)
    {
        size_t attrs; /* curses attributes */

        if (on) {
            wattron(this.window, (attrs = Screen.color("white_red")));
            mvwprintw(this.window, this.height - 1, this.width - 5, " v ");
            wattroff(this.window, attrs);
        } else {
            wattron(this.window, (attrs = Screen.color("blue_red")));
            mvwhline(this.window, this.height - 1, this.width - 5, ACS_HLINE, 3);
            wattroff(this.window, attrs);
        }
    }

    private void updateArrowUp(bool on)
    {
        size_t attrs; /* curses attributes */

        if (on) {
            wattron(this.window, (attrs = Screen.color("white_red")));
            mvwprintw(this.window, 0, this.width - 5, " ^ ");
            wattroff(this.window, attrs);
        } else {
            wattron(this.window, (attrs = Screen.color("blue_red")));
            mvwhline(this.window, 0, this.width - 5, ACS_HLINE, 3);
            wattroff(this.window, attrs);
        }
    }

    private bool move(int key)
    {
        bool need_refresh = true;

        switch (key)
        {
        case 0:
            /* the windows keys generate two key presses, of which the first
            is a zero. flush the buffer or the second key code will confuse
            everything. This happens here as all dialogs call this function
            after everything else. */
            flushinp();
            break;

            /* ^left */
        case 541: /* NCurses - Linux */
        case 443: /* PDCurses - Windows */
            if (this.x1 > 0) this.x1--;
            break;

            /* ^right */
        case 556: /* NCurses - Linux */
        case 444: /* PDCurses - Windows */
            if (this.x1 < (COLS - this.width)) this.x1++;
            break;

            /* ^up */
        case 562: /* NCurses - Linux */
        case 480: /* PDCurses - Windows */
            if (this.y1 > 0) this.y1--;
            break;

            /* ^down */
        case 521: /* NCurses - Linux */
        case 481: /* PDCurses - Windows */
            if (this.y1 < (LINES - this.height)) this.y1++;
            break;

        default:
            need_refresh = false;
        }

        if (need_refresh)
        {
            move_panel(this.panel, this.y1, this.x1);
            Display.paint(Game.get());
        }

        return need_refresh;
    }

    public static Window popup(uint _x1, uint _y1, uint _width, immutable string _title, immutable string _message) {
        const uint maxWidth = COLS - _x1 - 1;
        const uint maxHeight = LINES - _y1;

        if (_width == 0) {
            uint maxLen;

            /* The title is padded by 6 additional characters */
            if (_title && (_title.length + 6 > _message.length)) {
                maxLen = cast(uint)_title.length + 6;
            } else {
                maxLen = cast(uint)_message.length;
            }

            /* determine window width */
            if (maxLen > (maxWidth - 4)) {
                _width = maxWidth - 4;
            } else {
                _width = maxLen + 4;
            }
        } else {
            /* width supplied. sanity check */
            if (_width > maxWidth) {
                _width = maxWidth;
            }
        }

        string[] text = splitLines(wrap(_message, _width - 2));
        uint height = min(text.length + 2, maxHeight);

        Window popup = new Window(_x1, _y1, _width, height, _title);

        /* display message */
        size_t attrs;
        wattron(popup.window, (attrs = Screen.color("white_red")));
        for (uint idx = 0; idx < text.length; idx++) {
            mvwprintw(popup.window, idx + 1, 1,
                    " %-*s ", _width - 4, text[idx].toStringz);
        }
        wattroff(popup.window, attrs);

        /* show the window */
        wrefresh(popup.window);

        return popup;
    }

    public static int showMessage(string title, string message, uint indent=0)
    {
        /* Number of columns required for
            a) the window border and the text margin
            b) the padding around the window */
        const uint wred = 4;

        /* default window width according to available screen space;
           padded by 2 chars on each side. Limit the window to 74 columns
           to ensure it doesn't become too wide.*/
        uint width = min(COLS - wred, 74);

        int key;

        size_t offset;

        /* wrap message according to width */
        string[] text;

        foreach (line; splitLines(message)) {
            string wrapped = wrap(line, width - wred, null, leftJustify("", indent));
            text ~= splitLines(wrapped);
        }

        /* determine the length of longest text line */
        ulong max_len;
        foreach (line ; text)
            max_len = max(max_len, line.length);

        /* shrink the window width if the default width is not required */
        if (max_len + wred < width)
            width = cast(uint)max_len + wred;

        /* set height according to message line count */
        uint height = min((LINES - 3), (text.length + 2));

        uint starty = (LINES - height) / 2;
        uint startx = cast(uint)(COLS - width) / 2;
        uint maxvis = min(text.length, height - 2);

        Window mwin = new Window(startx, starty, width, height, title);

        bool RUN = true;
        do
        {
            /* display the window content */
            for (uint idx = 0; idx < maxvis; idx++)
            {
                uint count;
                wattron(mwin.window, Screen.color("white_red"));
                count = mvwprintw(mwin.window, idx + 1, 1,
                                " %-*s ".toStringz, width - wred,
                                text[idx + offset].toStringz);

                wattroff(mwin.window, Screen.color("white_red"));
            }

            mwin.updateArrowUp(offset > 0);
            mwin.updateArrowDown((offset + maxvis) < text.length);

            wrefresh(mwin.window);
            key = getch();

            switch (key)
            {
            case 'k', '8', KEY_UP:
                if (offset > 0)
                    offset--;
                break;

            case '9', KEY_PPAGE, KEY_A3, 21 /* ^U */:
                if (offset > maxvis + 1)
                    offset -= maxvis;
                else
                    offset = 0;
                break;

            case '7', KEY_HOME, KEY_A1:
                offset = 0;
                break;

            case 'j', '2', KEY_DOWN:
                if (text.length > (maxvis + offset))
                    offset++;
                break;

            case '3', KEY_NPAGE, KEY_C3, 4 /* ^D */:
                offset = min((offset + maxvis - 1), text.length - maxvis);
                break;

            case '1', KEY_END, KEY_C1:
                offset = text.length - maxvis;
                break;

            default:
                /* perhaps the window shall be moved */
                if (!mwin.move(key))
                {
                    /* some other key -> close window */
                    RUN = false;
                }
            }
        }
        while (RUN);

        mwin.close;

        return key;
    }

    public static showHistory(History history)
    {
        string text;

        /* assemble reversed game log */
        foreach (entry; history) {
            text ~= format("%s: %s\n", entry.time, entry.message);
        }

        /* display the log */
        Window.showMessage("Message history", text,
            cast(uint)history.latest.time.length + 2);
    }

    public static string getDirection(immutable string title, string[] available)
    {
        // Position the window somewhere sensible
        uint width = cast(uint)max(9, title.length + 4);
        uint startx = (COLS / 2) - (width / 2);
        uint starty = (LINES / 2) - 4;

        Window win = new Window(startx, starty, width, 9, title);

        size_t attrs; // curses attributes
        wattron(win.window, (attrs = Screen.color("white_red")));
        mvwprintw(win.window, 3, 3, "\\|/");
        mvwprintw(win.window, 4, 3, "- -");
        mvwprintw(win.window, 5, 3, "/|\\");
        wattroff(win.window, attrs);

        wattron(win.window, (attrs = Screen.color("yellow_red")));

        foreach (direction; available) {
            // The center of the directions is 4, 4
            Position dot = Position(4, 4).move(direction).move(direction);

            // fix the position of two labels
            if (direction == "nw" || direction == "sw")
                dot.x = dot.x - 1;

            mvwprintw(win.window, dot.y, dot.x, "%s", direction.toStringz);
        }

        wattroff(win.window, attrs);
        wrefresh(win.window);

        string direction = null;
        while (true)
        {
            int key = getch();

            if (key == 27 /* ESC */) {
                break;
            } else if (key in Screen.directionKeys) {
                string dir = Screen.getDirection(key);
                if (available.canFind(dir)) {
                    direction = dir;
                    break;
                }
            }
        }

        win.close();

        return direction;
    } // getDirection
}
