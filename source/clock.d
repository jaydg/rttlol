import std.json;
import std.string;

struct Clock {
    size_t msec;
    size_t sec;
    size_t min;
    size_t hour;
    size_t day;
    size_t year;

    this(JSONValue json) {
        msec = cast(size_t)json["msec"].integer;
        sec = cast(size_t)json["sec"].integer;
        min = cast(size_t)json["min"].integer;
        hour = cast(size_t)json["hour"].integer;
        day = cast(size_t)json["day"].integer;
        year = cast(size_t)json["year"].integer;
    }

    JSONValue toJSON() {
        JSONValue json;

        json["msec"] = JSONValue(msec);
        json["sec"] = JSONValue(sec);
        json["min"] = JSONValue(min);
        json["hour"] = JSONValue(hour);
        json["day"] = JSONValue(day);
        json["year"] = JSONValue(year);

        return json;
    }

    void advance(ulong msecs) {
        msec += msecs;

        if (msec >= 1000) {
            sec += msec / 1000;
            msec = msec % 1000;
        }

        if (sec >= 60) {
            min += sec / 60;
            sec = sec % 60;
        }

        if (min >= 60) {
            hour += min / 60;
            min  = min % 60;
        }

        if (hour >= 24) {
            day += hour / 24;
            hour = hour % 24;
        }

        if (day >= 365) {
            year += day / 365;
            day = day % 365 + 1;
        }
    }

    string toString() {
        return format("%02d:%02d:%02d", hour, min, sec);
    }

    float daylight() {
        switch ((hour * 60 + min) / 48) {
            // half hours
            case 0:  .. case 6:  return 0.1; // 0:00 - 5:00
            case 7:              return 0.3; // 5:00 - 5:30
            case 8:              return 0.5; // 5:30 - 6:00
            case 9:              return 0.7; // 6:00 - 6:30
            case 10:             return 0.9; // 6:30 - 7:00
            case 22:             return 0.9; // 19:00 - 19:30
            case 23:             return 0.7; // 19:30 - 20:00
            case 24:             return 0.5; // 20:00 - 20:30
            case 25:             return 0.3; // 20:30 - 21:00
            case 26: .. case 30: return 0.1; // 21:00 - 24:00
            default: return 1.0;
        }
    }
}
