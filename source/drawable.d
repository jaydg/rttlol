public interface Drawable
{
    public abstract char glyph();
    public abstract string color();
}
