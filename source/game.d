version (Posix) {
    import deimos.ncurses;
    import deimos.ncurses.panel;
}

version (Windows) {
    import pdcurses;
}

import std.algorithm: sort;
import std.experimental.logger;
import std.file;
import std.json;
import std.string;
import std.random;
import std.zlib;

import clock;
import display;
import display.screen;
import display.window;
import map;
import mobile.player;
import util.history;

public class Game
{
    public alias runfun = void delegate();

    private static Game game = null;
    private Map[string] maps;
    private uint seed;
    private Player _player;
    private bool _wizmode = false;
    private ulong _tick;
    private Clock _clock;
    private runfun[][ulong] queue;
    private History _history;
    private static const string savegame = "rttlol.sav";

    private this() {};

    private this(uint seed) {
        logf(LogLevel.info, "Generating new game (seed: %d)", seed);
        this.seed = seed;
        rndGen.seed(this.seed);
        this._history = new History();

        _player = new Player();
        // FIXME: deal with null startingPosition
        _player.setMap(map("larn"), map("larn").startingPosition);

        this.history.write!"Welcome to rttlol!";
    }

    private this(JSONValue json) {
        this.seed = cast(uint)json["seed"].integer;
        this._wizmode = json["wizmode"].type() == JSONType.true_;
        this._tick = json["tick"].integer;
        this._clock = Clock(json["clock"]);
        this._history = new History(json["history"]);

        foreach (map; json["maps"].array) {
            Map m = new Map(map);
            this.maps[m.name] = m;
        }
        this._player = new Player(json["player"], this.maps);
    }

    public Map map(string name) {
        if (! (name in maps)) {
            // try to generate the map if it doesn't exists
            maps[name] = Map.create(name);
        }
        return maps[name];
    }

    @property {
        public bool wizmode() {
            return _wizmode;
        }

        public Player player() {
            return _player;
        }

        public const Clock clock() {
            return _clock;
        }

        public ulong tick() {
            return _tick;
        }

        public History history() {
            return _history;
        }
    }

    public static Game get() {
        static bool creating = false;
        if (game is null) {
            // ensure creation of the game is not attempted subsequently
            if (creating) {
                return null;
            }

            creating = true;
            if (exists(savegame) && isFile(savegame)) {
                log(LogLevel.info, "Loading saved game.");
                game = load();
            } else {
                log(LogLevel.info, "Creating new game.");
                game = new Game(unpredictableSeed);
            }

            // initial
            game.player.calculateFov();

            creating = false;
        }

        return game;
    }

    JSONValue toJSON() {
        JSONValue[] jmaps;
        foreach (map; maps)
            jmaps ~= map.toJSON;

        JSONValue[string] json;
        json["seed"] = JSONValue(seed);
        json["wizmode"] = JSONValue(_wizmode);
        json["tick"] = JSONValue(_tick);
        json["clock"] = _clock.toJSON;
        json["history"] = _history.toJSON;
        json["player"] = _player.toJSON;
        json["maps"] = jmaps;

        return JSONValue(json);
    }

    private static Game load() {
        import std.conv : to;

        // FIXME: error handling
        auto decompressor = new UnCompress();
        void[] uncompressedJSON;
        uncompressedJSON ~= decompressor.uncompress(std.file.read(savegame));
        uncompressedJSON ~= decompressor.flush();
        string json = to!string(uncompressedJSON);

        // delete the file
        remove(savegame);

        return new Game(parseJSON(json));
    }

    public bool save(bool showPopup=false) {
        log(LogLevel.info, "Saving game");

        Window popup;
        if (showPopup) {
            popup = Window.popup(3, 3, 0, null, "Saving....");
        }

        auto jsonValue = this.toJSON();

        // TODO: exception handling
        void[] compressedJSON;
        auto compressor = new Compress(HeaderFormat.gzip);
        compressedJSON ~= compressor.compress(jsonValue.toString);
        compressedJSON ~= compressor.flush();
        std.file.write(savegame, compressedJSON);

        if (popup) {
            popup.close();
        }

        return true;
    }

    public void enqueue(ulong duration, runfun fun) {
        ulong tick = this._tick + duration;
        if (! tick in queue) {
            this.queue[tick] = [fun];
        } else {
            this.queue[tick] ~= fun;
        }
    }

    private void runQueue() {
        if (this.queue.length == 0) {
            return;
        }

        do {
            // get the next tick with queued actions
            ulong[] ticks = this.queue.keys;
            sort(ticks);
            ulong next_tick = ticks[0];

            logf(LogLevel.trace, "Current tick: %d, next tick: %d (%d actions queued)",
                this._tick, next_tick, this.queue[next_tick].length);

            foreach(task; this.queue[next_tick]) {
                task();
            }

            // all done!
            this.queue.remove(next_tick);

            // advance game tick
            this._clock.advance(next_tick - this._tick);
            this._tick = next_tick;
            player.calculateFov();
            Display.paint(this);

            // schedule moves for all inactive mobiles on map
            player.map.moveMonsters();
        } while (!Game.get.player.canMove);
    }

    public void run() {
        log(LogLevel.info, "Entering main game loop.");
        bool running = true;

        // initial
        Display.paint(this);

        do {
            // show the last message for up to five turns
            HistoryEntry last = history.latest;
            ulong displayPeriod = 5 * (player.map.at(player.pos).movementCost * player.map.scale);
            Display.message(format(" %10s (%02d, %02d) %s",
                    clock, player.pos.x, player.pos.y,
                    last is null ? "" : (last.tick + displayPeriod >= game.tick ? last.message : "")
                    ));

            int key = Display.get_key();
            bool advanceTime = false;
            string direction = null;
            switch (key) {
                case ' ': // space
                    // TODO: move out of here
                    import feature;
                    import position;
                    Feature f;

                    if (player.map.at(player.pos).feature) {
                        // feature on player's position
                        f = player.map.at(player.pos).feature;
                    } else {
                        // look for adjacent features
                        import tile;

                        string[] dirs = player.map.getSurrounding(player.pos, Tile.Element.FEATURE);

                        if (dirs.length == 1) {
                            f = player.map.at(Position(player.pos).move(dirs[0])).feature;
                        } else if (dirs.length > 1) {
                            string dir = Window.getDirection("Choose a direction", dirs);
                            if (dir) {
                                f = player.map.at(Position(player.pos).move(dir)).feature;
                            }
                        }
                    }

                    if (f) {
                        // FIXME: variable time
                        player.enqueueAction(1000, {
                            f.activate(player);
                        });
                        advanceTime = true;
                    }
                    break;
                case 'Q':
                    running = false; break;

                case 'S':
                    // saved successfully? terminate the game.
                    running = !this.save();
                    break;

                case 12: // ^L
                    Display.redraw(this);
                    break;

                case 18: /* ^R */
                    Window.showHistory(this.history);
                    break;

                case 23: // ^W
                    _wizmode = !_wizmode;
                    Display.paint(this);
                    break;

                case ';':
                    Display.getPosition(player.pos, "Choose a position to examine");
                    break;

                default:
                    if (key in Screen.directionKeys) {
                        direction = Screen.getDirection(key);
                    }
                    break;
            }

            if (direction != null) {
                player.move(direction);
                advanceTime = true;
                direction = null;
            }

            if (advanceTime) {
                runQueue();
            }
        } while (running);

        log(LogLevel.info, "Leaving main game loop.");
    }
}

shared static this() {
    // This is a share module constructor,
    // it is run once at the start of the program
    string filename = thisExePath() ~ ".log";
    sharedLog = new FileLogger(filename);
    log(LogLevel.info, "Application start.");
}

static int main(string[] args)
{
    scope(exit) {
        Display.terminate();
    }

    Game g = Game.get();
    Display.init();
    g.run();

    return 0;
}
